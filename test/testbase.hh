#include <argparser.hh>
#include <vector>
#include <unordered_map>
#include <any>
#include <sstream>
#include <iomanip>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <mpi.h>

class TestBase {
private:
  int _testbase_mpirank, _testbase_mpisize;

protected:
  std::shared_ptr<spdlog::logger> logger, funclogger;
  static argparser::ArgParser* argparser;
  static std::shared_ptr<argparser::Argument<bool>> arg_info;
  static std::shared_ptr<argparser::Argument<bool>> arg_debug;

  TestBase( int argc, char* argv[] ) {
    std::list<std::shared_ptr<argparser::ArgumentBase>> addlargs{};
    init( argc, argv, addlargs );
  }

  TestBase( int argc, char* argv[],
            std::list<std::shared_ptr<argparser::ArgumentBase>> &addlargs ) {
    if ( argparser != nullptr )
      throw std::runtime_error( "Tried to add arguments after they'd been parsed." );
    init( argc, argv, addlargs );
  }

  void init( int argc, char* argv[],
             std::list<std::shared_ptr<argparser::ArgumentBase>> &addlargs ) {
    if ( argparser == nullptr ) {
      argparser = argparser::ArgParser::get();
      arg_info = std::make_shared<argparser::Argument<bool>>( "-v", "", "info", false,
                                                              "Show info messages (default: just error & warning)" );
      arg_debug = std::make_shared<argparser::Argument<bool>>( "-vv", "", "debug", false, "Show debug messages" );
      argparser->add_arg( std::ref(*arg_info) );
      argparser->add_arg( std::ref(*arg_debug) );

      for ( auto ai=addlargs.begin() ; ai != addlargs.end() ; ++ai ) {
        argparser->add_arg( std::ref( *(*ai) ) );
      }

      argparser->parse( argc, argv );
    }

    MPI_Comm_rank( MPI_COMM_WORLD, &_testbase_mpirank );
    MPI_Comm_size( MPI_COMM_WORLD, &_testbase_mpisize );

    auto mylevel = spdlog::level::info;
    auto funclevel = spdlog::level::warn;

    if (arg_debug->given) {
      mylevel = spdlog::level::debug;
      funclevel = spdlog::level::debug;
    } else if ( arg_info->given) {
      funclevel = spdlog::level::info;
    }

    logger = spdlog::get( "test" );
    if ( logger == nullptr ) logger = spdlog::stderr_color_mt( "test" );
    logger->set_level( mylevel );
    funclogger = spdlog::get( "func" );
    if ( funclogger == nullptr ) funclogger = spdlog::stderr_color_mt( "func" );
    funclogger->set_level( funclevel );

    std::stringstream str("");
    if ( _testbase_mpisize == 1 ) {
      str << "[%Y-%m-%d %H:%M:%S] [%n] [%^%l%$] %v";
    } else {
      str << "[%Y-%m-%d %H:%M:%S] [%n] ["
          << std::setfill('0') << std::setw(2) << _testbase_mpirank
          << "] [%^%l%$] %v";
    }
    logger->set_pattern( str.str() );
    funclogger->set_pattern( str.str() );
    // Version of spdlog I have installed doesn't have this
    // logger->sinks()[0]->force_color_output();
    // funclogger->sinks()[0]->force_color_output();
  }

  int actually_run_tests( std::vector<std::string>& testnames,
                          std::unordered_map<std::string,std::function<bool()>>& tests ) {
    std::stringstream str("");
    int retval = 0;
    for ( auto testname: testnames ) {
      int iresult = 0;

      str.str("");
      // str << "Running test " << testname;
      // logger->debug( str.str() );
      auto test = tests[testname];
      str.str("");
      str << testname << " : ";
      bool exception = false;
      try {
        iresult = test() ? 0 : 1;
      }
      catch ( std::exception &ex ) {
        str << "exception: " << ex.what();
        iresult = 1;
        exception = true;
      }
      catch ( ... ) {
        str << "some kind of exception";
        iresult = 1;
        exception = true;
      }

      if ( exception ) {
        logger->error( str.str() );
      }
      MPI_Allreduce( MPI_IN_PLACE, &iresult, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
      retval = ( retval != 0 || iresult != 0 ) ? 1 : 0;

      if ( _testbase_mpirank == 0 ) {
        str.str("");
        str << testname << " : ";
        if ( iresult == 0 ) {
          str << "passed";
          logger->info( str.str() );
        } else {
          str << "failed";
          logger->error( str.str() );
        }
      }
    }
    return retval;
  }
};

argparser::ArgParser* TestBase::argparser = nullptr;
std::shared_ptr<argparser::Argument<bool>> TestBase::arg_info{ nullptr };
std::shared_ptr<argparser::Argument<bool>> TestBase::arg_debug{ nullptr };
