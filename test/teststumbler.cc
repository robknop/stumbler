#include "testbase.hh"
#include <stumbler.hh>
#include <string>
#include <fstream>
#include <iomanip>
#include <H5Cpp.h>
#include <mpi.h>

// I suspect this has memory leaks in mpi communicators...?

class TestStumbler : public TestBase {
protected:
  std::vector<double> x, y, dy;
  bool outhdf5 = false;
  std::string testname = "TestStumbler";

  int worldrank, worldsize;
  int totalranks, stumblerranks;

  // These values are fiddly, and depend on the rng
  // behaving exactly as expected given the seed.
  // This is for a single mpi rank.
  std::vector<double> expected_mean{ 0.641, 0.381 };
  std::vector<double> expected_var{ 0.162, 0.288 };
  std::vector<std::vector<double>> expected_covar{ { 0.162, -0.108 }, { -0.108, 0.288 } };
  double precision = 0.001;

  double lnL( const std::vector<double> &param, const MPI_Comm &comm ) {
    double x = param[0];
    double y = param[1];

    double r = sqrt( x*x + y*y );
    double θ = atan2( y, x );

    return -0.5 * ( (r-1.)*(r-1.) / 0.0025 + (θ - M_PI/6.)*(θ - M_PI/6.) / ( (M_PI/4.)*(M_PI/4.) ) );
  }

  virtual bool test_stumbler() {
    bool ok = true;
    std::stringstream str("");
    std::ios oldstate( nullptr );

    std::vector<double> initparam { 0.5, 0.6 };
    std::vector<std::string> paramnames { "x", "y" };
    std::vector<double> initsigma { 0.05, 0.05 };
    int stumbler_seed = 23;
    int nwalkers = 100;
    int burnin = 2000;
    int steps = 500;
    auto lnLfunc = std::bind( &TestStumbler::lnL, this, std::placeholders::_1, std::placeholders::_2 );
    std::string chainname, burnchainname, debugchainname;
    if ( outhdf5 ) {
      chainname = "teststumbler_chain.hdf5";
      burnchainname = "teststumbler_burnchain.hdf5";
      debugchainname = "teststumbler_debugchain.hdf5";
    }
    else {
      chainname = "teststumbler_chain.csv";
      burnchainname = "teststumbler_burnchain.csv";
      debugchainname = "teststumbler_debugchain.csv";
    }

    // str.str("");
    // str << "world rank " << worldrank << " making stumbler";
    // logger->warn( str.str() );

    MPI_Comm globalcomm, stumblercomm, subcomm;
    int worldmpirank, globalrank, stumblerrank, subrank;

    if ( totalranks % stumblerranks != 0 )
      throw std::runtime_error( "totalranks must be a multiple of stumblerranks" );
    int subranks = totalranks / stumblerranks;

    MPI_Comm_rank( MPI_COMM_WORLD, &worldmpirank );

    int color = worldmpirank < totalranks ? 0 : MPI_UNDEFINED;
    MPI_Comm_split( MPI_COMM_WORLD, color, worldmpirank, &globalcomm );
    if ( globalcomm != MPI_COMM_NULL ) {
      MPI_Comm_rank( globalcomm, &globalrank );
    } else {
      globalrank = -1;
    }


    if ( globalcomm != MPI_COMM_NULL ) {
      color = ( globalrank % subranks ) == 0 ? 1 : MPI_UNDEFINED;
      MPI_Comm_split( globalcomm, color, worldmpirank, &stumblercomm );
      if ( stumblercomm != MPI_COMM_NULL ) {
        MPI_Comm_rank( stumblercomm, &stumblerrank );
      } else {
        stumblerrank = -1;
      }

      color = globalrank / subranks;
      MPI_Comm_split( globalcomm, color, worldmpirank, &subcomm );
      MPI_Comm_rank( subcomm, &subrank );
    } else {
      stumblerrank = -1;
      subrank = -1;
    }

    // str.str("");
    // str << "MPI Ranks: world=" << worldrank
    //     << ", global=" << globalrank
    //     << ", stumbler=" << stumblerrank
    //     << ", sub=" << subrank;
    // logger->warn( str.str() );

    if ( globalcomm != MPI_COMM_NULL ) {
      auto stumbler = std::make_shared<Stumbler<double>>( nwalkers, burnin, steps, 2,
                                                          initparam, initsigma, paramnames, lnLfunc,
                                                          globalcomm, stumblercomm, subcomm,
                                                          chainname, burnchainname, debugchainname,
                                                          outhdf5, 100, stumbler_seed, funclogger );

      // str.str("");
      // str << "World rank " << worldrank << " going";
      // logger->warn( str.str() );

      stumbler->go( 100 );

      // str.str("");
      // str << "world rank " << worldrank << " done going";
      // logger->warn( str.str() );

      if ( globalrank == 0 ) {
        std::vector<std::vector<double>> mean_var, covar;
        stumbler->get_mean_var( mean_var );
        stumbler->get_covar( covar );

        oldstate.copyfmt( str );
        str << "Parameters:" << std::endl;
        for ( int i = 0 ; i < paramnames.size() ; ++i ) {
          str << std::setw(5) << paramnames[i] << " : "
              << std::setprecision(3) << std::fixed << std::setw(7)
              << mean_var[0][i] << " ± " << std::setw(7) << sqrt(mean_var[1][i]) << std::endl;
        }
        str << "\nCovariance matrix\n-----------------\n" << std::endl;
        for ( int i = 0 ; i < covar.size() ; ++i ) {
          for ( int j = 0 ; j < covar.size() ; ++j ) {
            str << std::scientific << std::setprecision(3)
                << std::setw(10) << covar[i][j] << "  ";
          }
          str << std::endl;
        }
        logger->debug( str.str() );
        str.copyfmt( oldstate );

        for ( int i = 0 ; i < paramnames.size() ; ++i ) {
          if ( ( fabs( mean_var[0][i] - expected_mean[i] ) > precision ) ||
               ( fabs( mean_var[1][i] - expected_var[i] ) > precision ) ) {
            oldstate.copyfmt( str );
            str.str("");
            str << testname << ": mean/variance " << i << " was "
                << std::fixed << std::setprecision(3) << mean_var[0][i] << ", " << mean_var[1][i]
                << ", doesn't match expected " << expected_mean[i] << ", " << expected_var[i];
            logger->error( str.str() );
            str.copyfmt( oldstate );
            ok = false;
          }
        }
        for ( int i = 0 ; i < paramnames.size() ; ++i ) {
          for ( int j = 0 ; j < paramnames.size() ; ++j ) {
            if ( fabs( covar[i][j] - expected_covar[i][j] ) > precision ) {
              oldstate.copyfmt( str );
              str.str("");
              str << testname << ": covar[" << i << "," << j << "] was "
                  << std::fixed << std::setprecision(3) << covar[i][j]
                  << ", doesn't match expected " << expected_covar[i][j];
              logger->error( str.str() );
              str.copyfmt( oldstate );
              ok = false;
            }
          }
        }
      }

      MPI_Comm_free( &subcomm );
      if ( stumblercomm != MPI_COMM_NULL )
        MPI_Comm_free( &stumblercomm );
      MPI_Comm_free( &globalcomm );
    }

    MPI_Bcast( &ok, 1, MPI_C_BOOL, 0, MPI_COMM_WORLD );
    return ok;
  }

  virtual bool test_readfile()
  {
    if ( worldrank == 0 )
      logger->warn( "test_readfile not implemented in this class" );
    return true;
  }

public:
  TestStumbler( int argc, char* argv[] )
    : TestBase( argc, argv )
  {
    MPI_Comm_rank( MPI_COMM_WORLD, &worldrank );
    MPI_Comm_size( MPI_COMM_WORLD, &worldsize );
    totalranks = 1;
    stumblerranks = 1;
  }

  virtual ~TestStumbler() = default;

  int run_tests() {
    if ( worldrank == 0 ) {
      std::stringstream str("");
      str << "Running tests for " << testname;
      logger->info( str.str() );
    }
    // logging sanity
    MPI_Barrier( MPI_COMM_WORLD );

    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_stumbler", std::bind( &TestStumbler::test_stumbler, this ) },
      { "test_readfile", std::bind( &TestStumbler::test_readfile, this ) }
    };
    std::vector<std::string> testnames{
      "test_stumbler",
      "test_readfile"
    };

    int retval = 0;
    retval = actually_run_tests( testnames, tests );

    // std::stringstream str("");
    // str << "World rank " << worldrank << " got " << retval << " from actually_run_tests";
    // logger->warn( str.str() );

    MPI_Allreduce( MPI_IN_PLACE, &retval, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );

    // str.str("");
    // str << "World rank " << worldrank << " returning retval " << retval;
    // logger->warn( str.str() );

    return retval;
  }
};


class TestStumblerHDF5 : public TestStumbler {
private:
  virtual bool test_readfile() {
    bool ok = true;

    if ( worldrank == 0 ) {
      std::stringstream str("");
      std::ios oldstate( nullptr );
      hid_t h5file = H5Fopen( "teststumbler_chain.hdf5", H5F_ACC_RDONLY, H5P_DEFAULT );
      hid_t dset = H5Dopen2( h5file, "chain", H5P_DEFAULT );
      hid_t space = H5Dget_space( dset );
      hsize_t ndims = H5Sget_simple_extent_ndims( space );
      if ( ndims != 2 ) {
        str.str("");
        str << "file has " << ndims << " dimensions, expected 2";
        logger->error( str.str() );
        ok = false;
      } else {
        hsize_t dims[2];
        hsize_t maxdims[2];
        H5Sget_simple_extent_dims( space, dims, maxdims );
        std::vector<double> databuffer( dims[0] * dims[1] );
        hid_t memspace= H5Screate_simple( ndims, dims, nullptr );
        hsize_t filestart[2]{ 0, 0 };
        H5Sselect_hyperslab( space, H5S_SELECT_SET, filestart, nullptr, dims, nullptr );
        H5Dread( dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, databuffer.data() );


        long double xbar = 0., ybar = 0.;
        long double xsqbar = 0, ysqbar = 0.;
        long double xy = 0;
        for ( int i = 0 ; i < dims[0] ; ++i ) {
          double x = databuffer[ i * dims[1] ];
          double y = databuffer[ i * dims[1] + 1 ];
          xbar += x;
          ybar += y;
          xsqbar += x*x;
          ysqbar += y*y;
          xy += x*y;
        }
        xbar /= dims[0];
        ybar /= dims[0];
        xsqbar = xsqbar / dims[0] - xbar*xbar;
        ysqbar = ysqbar / dims[0] - ybar*ybar;
        xy = xy / dims[0] - xbar*ybar;
        if ( ( fabs( xbar - expected_mean[0] ) > precision ) ||
             ( fabs( ybar - expected_mean[1] ) > precision ) ||
             ( fabs( xsqbar - expected_var[0] ) > precision ) ||
             ( fabs( ysqbar - expected_var[1] ) > precision ) ||
             ( fabs( xy - expected_covar[0][1] ) > precision ) ) {
          oldstate.copyfmt( str );
          str.str("");
          str << testname << ": mean/var/covar calculatd from output file didn't match expected:\n";
          str << "  <x> = " << xbar << ", expected " << expected_mean[0] << std::endl;
          str << "  <y> = " << ybar << ", expected " << expected_mean[1] << std::endl;
          str << "  σx² = " << xsqbar << ", expected " << expected_var[0] << std::endl;
          str << "  σy² = " << ysqbar << ", expected " << expected_var[1] << std::endl;
          str << "  σxy = " << xy << ", expected " << expected_covar[0][1] << std::endl;
          logger->error( str.str() );
          ok = false;

          H5Sclose( memspace );
        }
        H5Sclose( space );
        H5Dclose( dset );
        H5Fclose( h5file );
      }
    }

    MPI_Bcast( &ok, 1, MPI_C_BOOL, 0, MPI_COMM_WORLD );
    return ok;
  }

public:
  TestStumblerHDF5( int argc, char* argv[] )
    : TestStumbler( argc, argv )
  {
    outhdf5 = true;
    testname = "TestStumblerHDF5";
    totalranks = 1;
    stumblerranks = 1;
  }
};


class TestStumblerText : public TestStumbler {
public:
  TestStumblerText( int argc, char* argv[] )
    : TestStumbler( argc, argv )
  {
    outhdf5 = false;
    testname = "TestStumblerText";
    totalranks = 1;
    stumblerranks = 1;
  }
};


class TestStumblerHDF5MPI8 : public TestStumblerHDF5 {
public:
  TestStumblerHDF5MPI8( int argc, char* argv[] )
    : TestStumblerHDF5( argc, argv )
// class TestStumblerHDF5MPI8 : public TestStumbler {
// public:
//   TestStumblerHDF5MPI8( int argc, char* argv[] )
//     : TestStumbler( argc, argv )
  {
    outhdf5 = true;
    // outhdf5 = false;
    testname = "TestStumblerHDF5MPI8";
    MPI_Comm_rank( MPI_COMM_WORLD, &worldrank );
    MPI_Comm_size( MPI_COMM_WORLD, &worldsize );
    if ( worldsize != 8 ) {
      throw std::runtime_error( "Expected 8 mpi ranks" );
    }
    totalranks = 8;
    stumblerranks = 4;

    // Different values here because there are 4 rngs instead of 1
    expected_mean[0] = 0.637;
    expected_mean[1] = 0.370;
    expected_var[0] = 0.182;
    expected_var[1] = 0.286;
    expected_covar[0][0] = 0.181;
    expected_covar[0][1] = -0.113;
    expected_covar[1][0] = -0.113;
    expected_covar[1][1] = 0.286;
  }
};

// **********************************************************************

int main( int argc, char* argv[] )
{
  int provided = 0;
  int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
  assert( err == 0 );
  assert( provided == MPI_THREAD_MULTIPLE );

  int mpirank, mpisize;
  MPI_Comm_rank( MPI_COMM_WORLD, &mpirank );
  MPI_Comm_size( MPI_COMM_WORLD, &mpisize );


  int totret = 0;
  if ( true )
  {
    auto tester = TestStumblerHDF5( argc, argv );
    auto retval = tester.run_tests();
    totret += retval;
  }
  MPI_Barrier( MPI_COMM_WORLD );
  if ( true )
  {
    auto tester = TestStumblerText( argc, argv );
    auto retval = tester.run_tests();
    totret += retval;
  }
  MPI_Barrier( MPI_COMM_WORLD );
  if ( true )
  {
    auto tester = TestStumblerHDF5MPI8( argc, argv );
    auto retval = tester.run_tests();
    totret += retval;
  }

  MPI_Finalize();
  return( totret );
}
