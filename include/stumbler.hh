#ifndef INCLUDE_STUMBLER_HH
#define INCLUDE_STUMBLER_HH 1

#include <vector>
#include <list>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <gsl/gsl_rng.h>
#include <mpi.h>
#include <H5Cpp.h>
#include <spdlog/spdlog.h>

template<typename T> class StumblerChain;

/** An attempt at an implementation of a Goodman & Weare MCMC
 *
 * Create the Stumbler object, and then just call go.  When done, the
 * chain can be found in the property {@code chain} (in mpi rank 0
 * only!), which is a list of vectors.  Each element of the list has the
 * parameters for that link in the chain.
 *
 * Look at {@code test/teststumbler.cc} for an example.
 *
 * If {@code burnin > 0}, then system will make {@code burnin} steps
 * before saving steps to the chain.  If {@code burnin=0}, then both
 * {@code anneal} and {@code cluster} are ignored.
 *
 * If {@code anneal} is true (set by calling {@code set_anneal()}), then
 * the first half of the {@code burnin} steps will be a simulated
 * annealing, where the log liklihood is multiplied by β.  β will start
 * at {@code initbeta} and step up to 1 in {@code burnin/2} steps.  If
 * {@code anneal} is false, then both {@code initbeta} and {@code
 * cluster} are ignored.
 *
 * If {@code cluster} is true (set by calling {@code set_cluster}), when
 * the sampler reaches {@code 3/4 burnin} steps, it will attempt to
 * identfiy clusters of walker positions (by looking at their
 * liklihoods).  It will somehow choose one of the clusters, and then
 * reinitialize all walkers to be at the mean of that cluster,
 * distributed according to the standard deviation of the parameters of
 * those clusters.
 *
 * If {@code rutrecluster} is true (set by calling {@code
 * set_rutrecluster}, configured by calling @{set_rutlength}), then
 * during the burn-in phase the code will try to detect if it's in a
 * rut, and if so, will attempt to reinitialize the walkers similar to
 * what's described in the previous paragraph.
 *
 * (NOTE: {@code anneal}, {@code cluster}, and {@code rutrecluster}
 * haven't been tested in a long time!  They may be broken!)
 *
 * cf: Goodman & Weare, 2010CAMCS...5...65G
 * cf: https://www.astro.umd.edu/~miller/teaching/astrostat/lecture10.pdf
 * cf: Hou et al., 2012ApJ...745...198H
 * cf: Foreman-Mackey, Hogg, Lang, & Goodman, 2012, https://arxiv.org/abs/1202.3665
 *
 */
template<typename dtype>
class Stumbler {
public:

  /**
   * Stumbler constructor.
   *
   * @param nwalkers number of walkers.  Must be even.
   * @param burnin number of burnin steps
   * @param nsteps number of steps after burnin (chain length = nsteps * nwalkers)
   * @param stretchparam size of stretch move (2 is what people seem to use)
   * @param initparam mean initial value of parameters
   * @param initsigma initial parameters will be chosen from gaussians with these sigmas around initparam
   * @param paramnames names of parameters for output file (they should have no spaces)
   * @param loglikelihood a function that takes a vector of parameters
   *   and an MPI communicator, and returns the natural log of something
   *   proportional to the likelihood of those parameters ({@code
   *   ln(P(param|data))}, where {@code param} is the parameters and
   *   {@code data} is whatever data that's known by the {@code
   *   loglikelihood} function).  The (unspecified) contstant of
   *   proportionality must be constant.  The likelihood function can
   *   MPI parallelize itself over the passed MPI communicator.  (It
   *   should not parallelize itself over any other communicator,
   *   including MPI_COMM_WORLD!)
   * @param comm
   * @param stumblercomm
   * @param subcomm
   * @param chainname The filename of an HDF5 or CSV file to which the
   *   chain will be written (see below).  If this is "", no chain file
   *   is written.
   * @param burnchainname The filename of an HDF5 or CSV file to which
   *   the burn-in chain will be written.  It's in the same format as
   *   the file written to {@code chainname}, only with {@code
   *   nwalkers*burnin} rows.  If this is "". no burn-in chain file will
   *   be written.
   * @param debugchainname The filename of a HDF5 or CSV file to which
   *   the debug chain will be written.  This concatenates the burn-in
   *   and measurement chains, and also includes additional information.
   *   if this is "", no debug chain file will be written.
   * @param outhdf5 If true, will write files in HDF5 format.  If false,
   *   will write files in text CSV format.  HDF5 files are more
   *   efficient (both in writing time and disk space), but of course
   *   then more painful to read into random other codes.
   * @param dumpchainevery Update the output chain files with the chains each
   *   time this many steps have passed.  Also, update the working
   *   calculation of the means and covariances of all the parameters.
   *   Memory space is allocated to store {@code
   *   nwalkers*dumpchainevery} links in the chain.
   * @param seed seed to give to a gsl random number generator (note:
   *   default is *not* a system-random seed, but algorithm's default!
   *   Make your own system-random seed if you want one.)  Here so you
   *   can do reproducable tests.  (Note that if you run with different
   *   values of totalranks and numstumblers, you will not get identical
   *   results with the same seed!)
   * @param logger A spdlog logger object.  If left at nullptr, Stumbler
   *   will make one.  Use this if you want to customize the logging
   *   format, if you want to log to a level other than INFO, or if you
   *   want to merge the logging with some other logger.
   *
   * HDF5 format: For files named chainname and burnchainname, there
   *   will be a single dataset in the HDF5 named "chain" that is double
   *   array in row-major order (the HDF5 standard) with {@code nparam}
   *   columns and {@code nwalkers*nsteps) rows (or {@code
   *   nwalkers*burnin} rows for the burn chain).  The attribute
   *   "parameters" on this dataset is an array of C strings with the
   *   names from ({@code paramnames}).  Additional attributes: nwalkers
   *   : int, number of walkers burnin : int, number of burnin steps
   *   stretchparam : double, stretch move stretch parameter
   *
   *   The file named debugchainname will have additional datasets
   *   "proposed", "lnL", "z", "proposed_lnL', "walker", "other", and
   *   "accepted".  "chain" will be a row-major array with {@code
   *   nwalkers*(nsteps+burnin)} rows and {@code nparam} columns; the
   *   measurement chain is appended right after the burn-in chain, so
   *   you can treat the whole thing as one chain, or subdivide it
   *   however you want.  "proposed" is the same size as "chain", and
   *   represents the proposed parameters for this walker/step.
   *   "accepted" is an integer array of length {@code nwalkers*nsteps},
   *   which is 1 if the step was accepted or 0 if not.  (If accetped=1,
   *   then the corresponding row in "chain" and "proposed" will be the
   *   same.)  "lnL" is the log likelihood for the parameters in the
   *   corresponding row of "chain", and proposed_lnL is the log
   *   likelihood of the parmeters in the corresponding row of
   *   "proposed".  "z" and "other" indicate the origin walker and
   *   stretch factor used for this proposed step.
   *
   * CSV format: burnchainname and chainname just have a single header
   *   row with the names of all the non-fixed parameters, and then
   *   {@code nwalkers*nsteps} rows describing the links of the chains.
   *   Each row has space-separated numbers, the count of them equalling
   *   the number of non-fixed parameters.  debugchainname has lots more
   *   stuff, and is in the format needed for plot/explorechain.py.
   *
   * MPI Parallelization -- THIS SECTION IS OUT OF DATE, ROB FIX IT
   *
   * totalranks exists primarily for testing, but if for some reason you
   * don't want to use all of your available MPI ranks in your
   * calculation, set this to something smaller than the MPI size and
   * only this man of the ranks from MPI_COMM_WORLD (starting at 0) will
   * be used.  (Ideally, I'd pass a communicator here rather than a
   * number of ranks, but passing MPI communicators is sometimes
   * fraught.  As such, {@code Stumbler} must be created, and {@code
   * go()} must be called, by all of the ranks in MPI_COMM_WORLD, even
   * if you want to set totalranks to something other than the full
   * size.)
   *
   * By default, totalranks is the size of MPI_COMM_WORLD.  (You can
   * explicitly pass that, or pass -1 to have it set automatically.)
   *
   * By default, or when {@code numstumblers=1}, Stumbler will do its
   * calculations only in one MPI rank.  It will serially go through all
   * of the walkers each step.  This is useful if your loglikelihood
   * function itself is MPI parallelized; if it takes long enough and
   * parallelizes well itself, there may not be much benefit in trying
   * to MPI parallelize the loop over walkers within each MCMC step.
   * However, if {@code numstumbers} is greater than 1, then at each
   * step the walkers will be divided between that many processes.  In
   * that case, each process doing walkers will have a secondary
   * communicator associated with it, of size totalranks/numstumblers.
   * This secondary communicator will be passed to {@code
   * loglikelihood()}.  If {@code loglikelihood} does not efficiently
   * parallelize over MPI, then you want to make {@code numstumblers}
   * equal to totalranks.  (Note that you can't set numstumblers=-1 to
   * get this default; you have to explictly figure out the total number
   * of ranks in this case.)  In any event, numstumblers must divide
   * evenly into the totalranks.
   *
   * A use case for this might be if you have (say) 4 GPUs, but each
   * loglikelihood function needs to compute entirely within one GPU.
   * You could then set numstumblers=4, and then 4 loglikelihood
   * functions will be called at the same time.  (It's up to you,
   * however, to make sure that different processes running
   * loglikelihood use different GPUs....)  If, in this example case,
   * loglikelihood does not efficiently use MPI itself, you want to run
   * with {@code mpirun -np 4}.  Another use case would be where
   * loglikelihood is either pretty fast, or intrinsicially serial, in
   * which case you can get a speedup by having as many stumblers as
   * your MPI size.
   *
   * Ideally, the number of walkers is an integer multiple of 2 times
   * numstumblers.  If not, some ranks will have more work to do than
   * other ranks.
   *
   * Stumbler, and perhaps the loglikelihood funciton, will also use
   * OpenMP, so make sure that the total number of MPI ranks times
   * OMP_NUM_THREADS is not greater than the number of cores you have.
   */

  Stumbler( int nwalkers,
            int burnin,
            int nsteps,
            dtype stretchparam,
            const std::vector<dtype> &initparam,
            const std::vector<dtype> &initsigma,
            const std::vector<std::string> &paramnames,
            std::function<dtype(const std::vector<dtype>&, const MPI_Comm&)> loglikelihood,
            MPI_Comm& comm,
            MPI_Comm& stumblercomm,
            MPI_Comm& subcomm,
            const std::string &chainname,
            const std::string &burnchainname="",
            const std::string &debugchainname="",
            bool outhdf5=false,
            size_t dumpchainevery=100,
            unsigned long int seed=0,
            std::shared_ptr<spdlog::logger> logger=nullptr );
  ~Stumbler();

  /**
   * Specify whether each parameter is fixed or should be fit for.
   *
   * @param fixed is a vector whose length equals the number of parameters.  For each
   *   element, if true, it means that parameter will be fixed at its initial value
   *   rather than fit for.
   *
   * WARNING : this may not currently be properly implemented!
   *
   */
  void set_param_fixed( const std::vector<bool>& fixed );

  void set_anneal( bool );
  void set_initbeta( dtype f ) { initbeta = f; };
  void set_cluster( bool b ) { cluster = b; };
  void set_rutrecluster( bool b ) { rutrecluster = b; };
  void set_rutlength( int i ) { rutlength = i; }
  void set_no_mean_covar( bool val=true ) { do_not_calculate_covar = val; }
  void logconfig();

  /**
   * \brief A function called every {@code printevery} steps.
   *
   * When you call {@code go()}, it will print out a status report every
   * so many steps, specified in the argument to {@code go()}.  This
   * function, if given, will also be called at the same time.  It's
   * here so that you can print additional debugging information if you
   * wish.
   */
  void set_debugdump( std::function<void()> );

  /**
   * \brief Additional header information for the debug CSV file
   *
   * Additional lines that will be added to the top of the text debug
   * output file.  This is ignored if Stumbler was constructed with
   * either debugchainname="" or outhdf5=true.  A "#" will be added
   * to the beginning of each line, so that's not necessary;
   */
  void set_additional_debug_headers( const std::vector<std::string>  &hdrs ) { additional_debug_headers = hdrs; }

  /**
   * \brief Perform a single MCMC step.
   *
   * You generally do not want to call this directly!  I have to think
   * about how things are set up if anybody ever wants to do this; I
   * should probably make it private.  Instead, call {@code go()}.
   */
  void step();

  /**
   * \brief Run the MCMC.
   *
   * Will reinitialize the parameters, start over the output chain
   * files, and reseed the RNG.  Will then run for {@code nsteps+burnin}
   * steps (where those numbers are specified in the Stumbler
   * constructor).  Will print a timestamped status update every {@code
   * printevery} steps so you can monitor progress.
   *
   * (If you pass a logger to the {@code Stumbler} constructor that has
   * a log level of DEBUG, you will get much additional information at
   * the beginning, when reclustering happens, and when additional rows
   * are written to the output files.  There are some additional {@code
   * if ( false )} lines in {@code stumbler.cc} that you can change to
   * {@code if ( true )} if you want to get a truly overwhelming amount
   * of debugging information, with things printed out for every walker
   * every step.)
   *
   */
  void go( int printevery=1000 );

  /**
   * \brief Return mean and variance of all parameters
   *
   * This only makes sense to call after you've called {@code go()}.
   *
   * Pass a vector of vectors of doubles.  Its contents are replaced
   * with two elements.  The first element is the vector of means, the
   * second element is the vector of variances.
   *
   */
  void get_mean_var( std::vector<std::vector<dtype>> &mean_var );

  /**
   * \brief Return the covariance between the parameters
   *
   * This only makes sense to call after you've called {@code go()}.
   *
   */
  void get_covar( std::vector<std::vector<dtype>> &covar );

private:
  std::shared_ptr<spdlog::logger> logger;
  MPI_Datatype mpitype;
  MPI_Comm& comm;
  MPI_Comm& stumblercomm;
  MPI_Comm& subcomm;
  hid_t hdf5type;
  bool anneal=false, cluster=false, mpi_parallel_walkers=false, keepdebuginfo=false, rutrecluster=false;
  int rutlength=3;
  dtype initbeta=0.01;
  dtype beta, logbeta, logbetastep;
  bool reclustered;
  bool outhdf5;

  unsigned long int rngseed;
  gsl_rng *rng = NULL;

  const std::set<std::string> dtypewidenames{ "chain", "proposed" };
  const std::set<std::string> dtypenames{ "lnL", "z", "proposed_lnL" };
  const std::set<std::string> shortnames{ "walker", "other", "accepted" };
  const std::vector<std::string> debugparams{ "lnL", "proposed_lnL", "walker", "other", "z", "accepted" };
  std::map<std::string,std::unique_ptr<StumblerChain<dtype>>> dtypewidechains;
  std::map<std::string,std::unique_ptr<StumblerChain<dtype>>> dtypechains;
  std::map<std::string,std::unique_ptr<StumblerChain<short int>>> shortchains;

  // MPI_Comm& comm;
  // MPI_Comm& stumblercomm;
  // MPI_Comm& subcomm;
  int globalmpirank, globalmpisize, mpirank, mpisize, submpirank, submpisize;

  std::vector<bool> fixparam;
  std::vector<dtype> initparam;
  std::vector<dtype> initsigma;
  std::vector<std::string> paramnames;
  std::function<dtype(const std::vector<dtype>&, const MPI_Comm&)> loglikelihood;

  std::function<void()> debugdump;
  std::chrono::duration<double> steptime, dumptime, covartime;

  /**
   * param's outer index is walker, inner index is parameter
   *
   */
  std::vector<std::vector<dtype>> param;
  std::vector<std::vector<dtype>> newparam;
  std::vector<dtype> lnL;
  std::vector<dtype> newlnL;
  std::vector<short int> step_accepted;
  std::vector<short int> others;
  std::vector<short int> walkers;
  std::vector<dtype> zs;
  int curstep, wlkrsteps, naccepted, naccepted_at_burnin;

  std::vector<long double> working_mean;
  std::vector<std::vector<long double>> working_covar;

  std::vector<std::string> additional_debug_headers;

  dtype stretchparam, stretchx0, stretchxr;
  int nparam, burnin, nwalkers, nsteps;
  const std::string chainname;
  const std::string burnchainname;
  const std::string debugchainname;
  int dumpchainevery;
  bool do_not_calculate_covar;

  void create_chain_files();
  void create_chain_files_hdf5();
  void create_one_chain_file_hdf5( const std::string &filename, bool debug );
  void create_chain_files_txt();
  void create_one_chain_file_txt( const std::string &filename, bool debug );

  void dump_chains( int curstep );
  void dump_chains_to_hdf5_file( const std::string &filename, bool debug );
  void dump_chains_to_txt_file( const std::string &filename, bool debug );

  void reinitialize( unsigned long int seed=0 );
  void initialize_parameters();
  void recluster();
};

// **********************************************************************
// Don't use this outside of stumbler.cc

template<typename T>
class StumblerChain {
private:
  unsigned int length, nwalkers, width, cur;
  std::vector<T> vals;

  inline void append( const std::vector<T> &newvals ) {
    // I think std::vector takes care of this
    // if ( cur > length )
    //   throw std::range_error( "Tried to append to chain past end" );

    if ( newvals.size() != nwalkers * width )
      throw std::range_error( "Tried to append a length other than nwalkers*width" );

    std::copy( newvals.begin(), newvals.end(), vals.begin() + cur * nwalkers * width );
    cur += 1;
  }


  inline void append( const std::vector<std::vector<T>> &newvals ) {
    // I think std::vector takes care of this
    // if ( cur > length )
    //   throw std::range_error( "Tried to append to chain past end" );

    if ( newvals.size() != nwalkers )
      throw std::range_error( "Tried to append a length other than nwalkers" );

    auto dest = vals.begin() + cur * nwalkers * width;
    for ( auto ni = newvals.begin() ; ni != newvals.end() ; ++ni ) {
      if ( ni->size() != width )
        throw std::range_error( "Tried to append vals with size other than width" );
      std::copy( ni->begin(), ni->end(), dest );
      dest += width;
    }

    cur += 1;
  }

// Not sure why I need this to be public, but I was getting "is private"
// errors in the std::make_unique<StumblerChain<dtype>> calls in the
// Stumbler constructor, even though Stumbler is a friend.
public:
  StumblerChain( unsigned int length, unsigned int nwalkers, unsigned int width ) {
    this->length = length;
    this->width = width;
    this->nwalkers = nwalkers;
    vals.resize( length * nwalkers * width );
    cur = 0;
  }

  template<typename> friend class Stumbler;
};



#endif

