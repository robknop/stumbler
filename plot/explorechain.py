import sys
import re
import argparse
import pandas

import numpy
import numpy.fft

import PyQt5.QtCore as qtcore
import PyQt5.QtWidgets as qt
import PyQt5.QtGui as qtgui

import matplotlib
import matplotlib.figure
from matplotlib.backends.backend_qtagg import FigureCanvas
from matplotlib.backends.backend_qtagg import NavigationToolbar2QT as NavigationToolbar

# Stolen (and modified) from https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
def autocor( data, norm=True ):
    n = 1
    while n < len(data):
        n = n << 1

    f = numpy.fft.fft( data, n=2 * n)
    acf = numpy.fft.ifft(f * numpy.conjugate(f))[: len(data) ].real
    acf /= 4 * n

    if norm:
        acf /= acf[0]

    return acf


# Stolen from https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
def auto_window(taus, c):
    m = numpy.arange(len(taus)) < c * taus
    # I think we want a ~m here ; it wasn't in that URL
    if numpy.any( ~m ):
        return numpy.argmin(m)
    return len(taus) - 1


def binsearch( sorted_array, value ):
    """Given sorted_array and value, return the maximum index in
    sorted_array such that sorted_array[index]>value
    """
    if sorted_array.ndim != 1:
        raise RuntimeError( "sorted_array must be 1d" )
    low = 0
    high = sorted_array.shape[0]
    while high - low > 1:
        dex = ( low + high ) // 2
        if sorted_array[dex] > value:
            high = dex
        else:
            low = dex
    if sorted_array[low] > value:
        return low
    else:
        return high

# ======================================================================-
# Generic wrapper to wrap a list in a QAbstractListModel
# There must be a pre-existing Qt5 class that does this.

class ListWrapper( qtcore.QAbstractListModel ):
    def __init__( self, thelist, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        self.thelist = thelist

    # I don't really know what I'm doing here...
    # I'm not sure what index is for
    # def index( self, row, column, parent ):
    #     if column == 0:
    #         return self.createIndex( row, column, self.thelist[row] )
    #     else:
    #         raise ValueError( f"index got column {column}" )

    def rowCount( self, parent ):
        return len( self.thelist )

    def columnCount( self, parent ):
        return 0

    def data( self, dex, role ):
        if role == qtcore.Qt.DisplayRole:
            if ( dex.row() < 0 ) or ( dex.row() >= len( self.thelist ) ):
                raise ValueError( f"data got row {dex.row()}, which is outsize list size {len(self.thelist)}" )
            if ( dex.column() != 0 ):
                raise ValueError( f"data got column {dex.column()}, which is not 0" )
            return self.thelist[ dex.row() ]

# ======================================================================
# Parent class for plot windows

class PlotWindow( qt.QMainWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        self.explorer = explorer
        self.explorercheckwidget = None  # Override this

        mainwidget = qt.QWidget()
        self.setCentralWidget( mainwidget )
        self.mainvbox = qt.QVBoxLayout()
        mainwidget.setLayout( self.mainvbox )

        self.canvas = FigureCanvas( matplotlib.figure.Figure( figsize=(8, 8), tight_layout=True ) )
        self.mainvbox.addWidget( NavigationToolbar( self.canvas, self ) )
        self.mainvbox.addWidget( self.canvas, 1 )

        self.titles = { 'ΩM' : r'$\Omega_M$',
                        'γ' : r'$\gamma$',
                        'bD' : r'$BD$',
                        'duint' : r'$\delta u_\mathrm{int}\ (\mathrm{km\,s^{-1}})$',
                        'dmint' : r'$\delta m_\mathrm{int}$',
                        'x': r'$x$',
                        'y': r'$y$',
                        'fσ8': r'$f\sigma_8$'
                       }

    def closeEvent( self, ev ):
        super().hide()
        if self.explorercheckwidget is not None:
            self.explorercheckwidget.setChecked( False )

    def hide( self, *args, **kwargs ):
        super().hide( *args, **kwargs )
        self.explorer.removeParamListener( self.update_plot )

    def show( self, *args, **kwargs ):
        super().show( *args, **kwargs )
        self.explorer.addParamListener( self.update_plot )
        self.update_plot()

    # override this
    def update_plot( self ):
        pass

# ======================================================================
# Corner plot window

class CornerPlot( PlotWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( explorer, *args, **kwargs )

        self.proposedsteps = []

        self.explorercheckwidget = self.explorer.showcorner
        self.setWindowTitle( "Corner Plot" )

        subwindow = qt.QWidget()
        self.mainvbox.addWidget( subwindow, 0 )
        hbox = qt.QHBoxLayout()
        subwindow.setLayout( hbox )

        self.showchain = qt.QRadioButton( "chain", subwindow )
        hbox.addWidget( self.showchain, 0 )
        self.showburn = qt.QRadioButton( "burn chain", subwindow )
        hbox.addWidget( self.showburn, 0 )
        self.showfull = qt.QRadioButton( "full chain", subwindow )
        hbox.addWidget( self.showfull, 0 )
        self.showsteps = qt.QRadioButton( "steps", subwindow )
        hbox.addWidget( self.showsteps, 0 )
        self.startstep = qt.QLineEdit( "0" )
        hbox.addWidget( self.startstep, 1 )
        hbox.addWidget( qt.QLabel( "to" ), 0 )
        self.endstep = qt.QLineEdit( str( len(explorer.chain) // explorer.walkers ) )
        hbox.addWidget( self.endstep , 1 )
        self.showchain.setChecked( True )

        subwindow = qt.QWidget()
        self.mainvbox.addWidget( subwindow, 0 )
        hbox = qt.QHBoxLayout()
        subwindow.setLayout( hbox )

        self.greyscale = qt.QCheckBox( "Greyscale" )
        self.greyscale.setChecked( True )
        hbox.addWidget( self.greyscale, 0 )
        self.showstep = qt.QCheckBox( "Show walkers at step " )   # I am a terrible person
        self.showstep.setChecked( False )
        hbox.addWidget( self.showstep, 0 )

        self.steptoshow = qt.QLineEdit( str( len(explorer.chain) // explorer.walkers - 1 ) )
        hbox.addWidget( self.steptoshow, 1 )
        button = qt.QPushButton( "+" )
        hbox.addWidget( button )
        button.clicked.connect( self.incrementsteptoshow )
        button.setMaximumWidth( button.fontMetrics().boundingRect( "+" ).width()*2 )
        button = qt.QPushButton( "−" )
        button.setMaximumWidth( button.fontMetrics().boundingRect( "+" ).width()*2 )
        hbox.addWidget( button )
        button.clicked.connect( self.decrementsteptoshow )

        subwindow = qt.QWidget()
        self.mainvbox.addWidget( subwindow, 0 )
        hbox = qt.QHBoxLayout()
        subwindow.setLayout( hbox )

        self.showprop = qt.QCheckBox( "Show proposed step for walker" )
        self.showprop.setChecked( False )
        hbox.addWidget( self.showprop, 0 )
        self.proptoshow = qt.QLineEdit( "0" )
        self.proptoshow.setValidator( qtgui.QIntValidator( 0, self.explorer.walkers, subwindow ) )
        hbox.addWidget( self.proptoshow, 1 )
        button = qt.QPushButton( "+" )
        button.setMaximumWidth( button.fontMetrics().boundingRect( "+" ).width()*2 )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.incrementpropwalker )
        button = qt.QPushButton( "−" )
        button.setMaximumWidth( button.fontMetrics().boundingRect( "+" ).width()*2 )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.decrementpropwalker )
        self.lnLinfolabel = qt.QLabel( "" )
        hbox.addWidget( self.lnLinfolabel, 1 )

        self.update_plot()

        for editedconnect in ( self.startstep, self.endstep, self.steptoshow ):
            editedconnect.editingFinished.connect( self.update_plot )
        for toggleconnect in ( self.showchain, self.showburn, self.showfull, self.showsteps,
                               self.greyscale, self.showstep, self.showprop ):
            toggleconnect.toggled.connect( self.update_plot )

        self.showprop.toggled.connect( lambda ev: self.update_proposed_arrows() )
        self.proptoshow.editingFinished.connect( lambda ev: self.update_proposed_arrows() )

        self.explorer.addParamListener( self.update_plot )

    def incrementsteptoshow( self, ev ):
        cur = int( self.steptoshow.text() )
        cur += 1
        if cur < 0: cur = 0
        nwalkers = len(self.explorer.chain) // self.explorer.walkers
        if cur >= nwalkers: cur = nwalkers - 1
        self.steptoshow.setText( str( cur ) )
        # I'd like to trigger the editingFinished signal of self.steptoshow,
        #  but I haven't figured out how to do that manually
        self.update_plot()

    def decrementsteptoshow( self, ev ):
        cur = int( self.steptoshow.text() )
        cur -= 1
        if cur < 0: cur = 0
        nwalkers = len(self.explorer.chain) // self.explorer.walkers
        if cur >= nwalkers: cur = nwalkers - 1
        self.steptoshow.setText( str( cur ) )
        self.update_plot()

    def incrementpropwalker( self, ev ):
        cur = int( self.proptoshow.text() )
        cur += 1
        if cur < 0: cur = 0
        if cur >= self.explorer.walkers: cur = self.explorer.walkers
        self.proptoshow.setText( str( cur ) )
        self.update_proposed_arrows()

    def decrementpropwalker( self, ev ):
        cur = int( self.proptoshow.text() )
        cur -= 1
        if cur < 0: cur = 0
        if cur >= self.explorer.walkers: cur = self.explorer.walkers
        self.proptoshow.setText( str( cur ) )
        self.update_proposed_arrows()

    def step_to_show( self ):
        showstep = self.showstep.isChecked()
        steptoshow = int( self.steptoshow.text() )
        showdexfirst = self.explorer.walkers * steptoshow
        showdexlast = showdexfirst + self.explorer.walkers
        if ( steptoshow < 0 ): steptoshow = 0
        if ( steptoshow >= len( self.explorer.chain ) ): steptoshow = len( self.explorer.chain ) - 1

        return showstep, steptoshow, showdexfirst, showdexlast

    def update_plot( self ):
        sys.stderr.write( "...plotting corner...\n" )
        fig = self.canvas.figure
        fig.clf()
        self.axes = []

        firstoff = 0
        lastoff = len( self.explorer.chain )
        if ( self.showchain.isChecked() ):
            firstoff = self.explorer.walkers * self.explorer.burnsteps
            lastoff = len( self.explorer.chain )
        elif ( self.showburn.isChecked() ):
            firstoff = 0
            lastoff = self.explorer.walkers * self.explorer.burnsteps
        elif ( self.showsteps.isChecked() ):
            firstoff = int( self.startstep.text() )
            lastoff = int( self.endstep.text() )
            if ( firstoff < 0 ): firstoff = 0
            if ( firstoff >= len( self.explorer.chain ) ): firstoff = len( self.explorer.chain ) - 1
            if ( lastoff <= firstoff ): lastoff = firstoff + 1
            if ( lastoff > len( self.explorer.chain ) ): lastoff = len( self.explorer.chain )
            firstoff *= self.explorer.walkers
            lastoff *= self.explorer.walkers

        fulldf = self.explorer.chain
        df = self.explorer.chain[ firstoff:lastoff ]

        plotlimits = None
        nbins = 20

        points = False
        contour = True
        greyscale = self.greyscale.isChecked()

        showstep, steptoshow, showdexfirst, showdexlast = self.step_to_show()

        columns = [ df.columns[i.row()] for i in self.explorer.paramswid.selectedIndexes() ]
        self.columns = columns

        linestyle = ""
        titlesize = 20
        axistitlesize = 18
        labelsize = 10
        datamarkersize = 5
        stepmarkersize = 0.2
        bigmarkersize = 3.0
        bigmarkercolor = "royalblue"
        pointcolor = "royalblue" if ( not ( contour or greyscale ) ) else "lavender"
        contourcolor = "maroon"
        histcolor = "maroon"

        if greyscale and points:
            sys.stderr.write( "Warning: setting points to False since greyscale will completely cover them\n" )
            points = False

        n = len( columns )

        if plotlimits is None:
            plotlimits = [ None ] * len(columns)

        for i in reversed( range( len(columns) ) ):
            coli = df[columns[i]].values
            fullcoli = fulldf[columns[i]].values
            fullpropi = fulldf[f"prop_{columns[i]}"].values
            for j in range( i, len(columns) ):
                colj = df[columns[j]].values
                fullcolj = fulldf[columns[j]].values
                fullpropj = fulldf[f"prop_{columns[j]}"].values
                ax = fig.add_subplot( n, n, j*n + i + 1 )
                self.axes.append( ax )
                ax.tick_params( axis='both', labelsize=labelsize )

                if j == i:
                    plotrange = ( plotlimits[i][0], plotlimits[i][1] ) if plotlimits[i] is not None else None
                    vals, bins = numpy.histogram( coli, range=plotrange, bins=nbins )

                    ax.step( bins[:-1], vals, where='pre', color=histcolor )
                    if ( j == len(columns)-1 ):
                        title = self.titles[columns[i]] if i in self.titles else f'${columns[i]}$'
                        ax.set_xlabel( title, fontsize=axistitlesize )
                    if ( i == 0 ):
                        ax.set_ylabel( '$N$', fontsize=axistitlesize )
                    if plotlimits[i] is not None:
                        ax.set_xlim( plotlimits[i][0], plotlimits[i][1] )
                    else:
                        plotlimits[i] = ax.get_xlim()

                else:
                    plotrange = [ [ plotlimits[i][0], plotlimits[i][1] ],
                                  [ plotlimits[j][0], plotlimits[j][1] ] ]
                    ax.set_xlim( plotlimits[i][0], plotlimits[i][1] )
                    ax.set_ylim( plotlimits[j][0], plotlimits[j][1] )
                    if points:
                        ax.plot( coli, colj, marker='o', markersize=stepmarkersize, linestyle=linestyle,
                                 color=pointcolor, markerfacecolor=pointcolor, zorder=2 )
                    hist, xbins, ybins = numpy.histogram2d( coli, colj, range=plotrange, bins=nbins )
                    xvals = ( xbins[:-1] + xbins[1:] ) / 2.
                    yvals = ( ybins[:-1] + ybins[1:] ) / 2.
                    if contour:
                        # Figure out confidence intervals
                        vals = numpy.flip( numpy.sort( hist.flatten().copy() ) )
                        cumulvals = numpy.cumsum( vals / vals.sum() )
                        quantiles = [ 0.9973, 0.9545, 0.6827 ]
                        contours = []
                        for q in quantiles:
                            dex = binsearch( cumulvals, q )
                            contours.append( vals[dex] )
                        ax.contour( xvals, yvals, hist.T, levels=contours, zorder=4, colors=contourcolor )
                    if greyscale:
                        ax.imshow( numpy.flipud(hist.T), extent=( xbins[0], xbins[-1], ybins[0], ybins[-1] ),
                                   interpolation="None", cmap="Greys", aspect="auto", zorder=3 )
                    if showstep:
                        ax.plot( fullcoli[showdexfirst:showdexlast], fullcolj[showdexfirst:showdexlast], marker='o',
                                 markersize=bigmarkersize, linestyle="None",
                                 color=bigmarkercolor, markerfacecolor=bigmarkercolor, zorder=5 )
                    ax.tick_params( axis='both', labelsize=labelsize )
                    if ( j == len(columns)-1 ):
                        title = self.titles[columns[i]] if i in self.titles else f'${columns[i]}$'
                        ax.set_xlabel( title, fontsize=axistitlesize )
                    if ( i == 0 ):
                        title = self.titles[columns[j]] if j in self.titles else f'${columns[j]}$'
                        ax.set_ylabel( title, fontsize=axistitlesize )


        self.update_proposed_arrows( showstep, steptoshow, showdexfirst, showdexlast, True )
        fig.canvas.draw()

        # Todo : put this in the interface somewhere

        means = df[columns].mean()
        sdevs = df[columns].std()
        sys.stderr.write( "\nMeans:\n" )
        for col in columns:
            sys.stderr.write( f'{col:6s} : {means.loc[col]:9.3f} ± {sdevs.loc[col]:9.3f}\n' )
        sys.stderr.write( f"\nCovariance matrix:\n{df[columns].cov()}\n" )
        sys.stderr.write( f"\nCorrelation matrix:\n{df[columns].corr()}\n" )
            

    def update_proposed_arrows( self, showstep=None, steptoshow=None,
                                showdexfirst=None, showdexlast=None, nodraw=False ):
        # To really make this faster, I'd need to
        #   do some tricks beyond just canvas.draw()
        accarrowcolor = "lawngreen"
        rejarrowcolor = "orangered"

        if showstep is None:
            showstep, steptoshow, showdexfirst, showdexlast = self.step_to_show()

        fulldf = self.explorer.chain

        showprop = False
        if steptoshow < ( len(self.explorer.chain) // self.explorer.walkers ) - 1:
            showprop = self.showprop.isChecked()
            if showprop:
                proptoshow = int( self.proptoshow.text() )
                if proptoshow < 0: proptoshow = 0
                if proptoshow >= self.explorer.walkers: proptoshow = self.explorer.walkers - 1
                showpropfrom = showdexfirst + proptoshow
                showpropto = showpropfrom + self.explorer.walkers

        self.lnLinfolabel.setText( "" )
        if len(self.proposedsteps) > 0:
            for step in self.proposedsteps:
                step.remove()
            if not nodraw:
                self.canvas.figure.canvas.draw()
        self.proposedsteps = []

        if not showprop:
            return

        self.lnLinfolabel.setText( f"lnL from {fulldf['lnL'][showpropfrom]:.3g} "
                                   f"to {fulldf['prop_lnL'][showpropto]:.3g} "
                                   f"{'accepted' if fulldf.accepted[showpropto] else 'not accepted'}" )

        axdex = -1
        for i in reversed( range( len( self.columns ) ) ):
            fullcoli = fulldf[ self.columns[i] ].values
            fullpropi = fulldf[f"prop_{self.columns[i]}"].values
            for j in range( i, len( self.columns ) ):
                fullcolj = fulldf[ self.columns[j] ].values
                fullpropj = fulldf[f"prop_{self.columns[j]}"].values
                axdex += 1
                ax = self.axes[ axdex ]

                x = fullcoli[showpropfrom]
                y = fullcolj[showpropfrom]
                dx = fullpropi[showpropto] - x
                dy = fullpropj[showpropto] - y
                if j == i:
                    y = 0
                    dy = 0
                color = accarrowcolor if fulldf.accepted[showpropto] else rejarrowcolor
                # ****
                sys.stderr.write( f"Arrow for ({self.columns[i]},{self.columns[j]}) "
                                  f"at {x:.3g},{y:.3g}, length {dx:.3g},{dy:.3g}, color {color}\n" )
                # ****
                # ax.arrow( x, y, dx, dy, color=color, zorder=6,
                #           length_includes_head=True, width=.001, head_width=0.01, head_length=0.03 )
                artist = ax.annotate( "", xytext=(x, y), xy=(x+dx, y+dy),
                                      arrowprops={ 'arrowstyle': "->", "linewidth": 2, "color": color,
                                                   'shrinkA': 0, 'shrinkB': 0 }, zorder=6 )
                self.proposedsteps.append( artist )

        if not nodraw:
            self.canvas.figure.canvas.draw()

# ======================================================================

class ParamVsStepPlot( PlotWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( explorer, *args, **kwargs )

        self.explorercheckwidget = self.explorer.showparamvsstep
        self.setWindowTitle( "Params Vs. Time" )

        subwindow = qt.QWidget()
        self.mainvbox.addWidget( subwindow, 0 )
        hbox = qt.QHBoxLayout()
        subwindow.setLayout( hbox )

        self.allwalkers = qt.QRadioButton( "All Walkers", subwindow )
        hbox.addWidget( self.allwalkers, 0 )
        self.allwalkers.toggled.connect( self.update_plot )
        self.onewalker = qt.QRadioButton( "Walker #", subwindow )
        hbox.addWidget( self.onewalker, 0 )
        self.onewalker.setChecked( True )
        self.onewalker.toggled.connect( self.update_plot )
        self.whichwalker = qt.QLineEdit( '0' )
        self.whichwalker.setValidator( qtgui.QIntValidator( 0, self.explorer.walkers, subwindow ) )
        self.whichwalker.editingFinished.connect( self.update_plot )
        hbox.addWidget( self.whichwalker )
        button = qt.QPushButton( "+" )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.incrementwalker )
        button = qt.QPushButton( "−" )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.decrementwalker )

        self.update_plot()

    def incrementwalker( self, ev ):
        cur = int( self.whichwalker.text() )
        cur += 1
        if cur < 0: cur = 0
        if cur > self.explorer.walkers: cur = self.explorer.walkers - 1
        self.whichwalker.setText( str( cur ) )
        self.update_plot()

    def decrementwalker( self, ev ):
        cur = int( self.whichwalker.text() )
        cur -= 1
        if cur < 0: cur = 0
        if cur > self.explorer.walkers: cur = self.explorer.walkers - 1
        self.whichwalker.setText( str( cur ) )
        self.update_plot()

    def update_plot( self ):
        sys.stderr.write( "...plotting param vs t...\n" )
        fig = self.canvas.figure
        fig.clf()

        linestyle = "solid"
        linecolor = "royalblue"
        burnlinestyle = "dashed"
        burnlinecolor = "maroon"
        titlesize = 20
        axistitlesize = 18
        labelsize = 10

        params = [ self.explorer.chain.columns[ i.row() ] for i in self.explorer.paramswid.selectedIndexes() ]

        if self.allwalkers.isChecked():
            df = self.explorer.chain
            stepnums = numpy.array( range( len(df) ) ) / self.explorer.walkers
        else:
            walker = int( self.whichwalker.text() )
            if walker < 0: walker = 0
            if walker >= self.explorer.walkers: walker = self.explorer.walkers-1
            df = self.explorer.chain[walker::self.explorer.walkers]
            stepnums = range( len(df) )

        burnstep = self.explorer.burnsteps

        for i, param in enumerate( params ):
            ax = fig.add_subplot( len(params), 1, i+1 )
            ax.tick_params( axis='both', labelsize=labelsize )
            ax.set_xlabel( "Step", fontsize=axistitlesize )
            lab = self.titles[ param ] if param in self.titles else f'${param}$'
            ax.set_ylabel( lab, fontsize=axistitlesize )
            ax.plot( stepnums, df[ param ].values, linestyle=linestyle, color=linecolor )
            ax.plot( [ burnstep, burnstep ], ax.get_ylim(), linestyle=burnlinestyle, color=burnlinecolor )

        fig.canvas.draw()

# ======================================================================

class AcceptanceVsStepPlot( PlotWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( explorer, *args, **kwargs )

        self.setWindowTitle( "Acceptance vs. Step" )
        self.explorercheckwidget = self.explorer.showacceptvst

        self.update_plot()

    def hide( self, *args, **kwargs ):
        super().hide( *args, **kwargs )

    def show( self, *args, **kwargs ):
        super().show( *args, **kwargs )

    def update_plot( self ):
        sys.stderr.write( f"Chain columns: {self.explorer.chain.columns}\n" )
        sys.stderr.write( f"{self.explorer.chain['accepted']}\n" )
        accepted = ( self.explorer.chain['accepted'].groupby( self.explorer.chain.index.values
                                                            // self.explorer.walkers ).apply( sum )
                     / self.explorer.walkers )

        linestyle = "solid"
        linecolor = "royalblue"
        burnlinestyle = "dashed"
        burnlinecolor = "maroon"
        axistitlesize = 18
        labelsize = 10

        fig = self.canvas.figure
        fig.clf()
        ax = fig.add_subplot( 1, 1, 1 )
        ax.tick_params( axis='both', labelsize=labelsize )
        ax.set_xlabel( "Step", fontsize=axistitlesize )
        ax.set_ylabel( "Acceptance fraction", fontsize=axistitlesize )
        ax.plot( range(len(accepted)), accepted.values, linestyle=linestyle, color=linecolor )
        ax.plot( [ self.explorer.burnsteps, self.explorer.burnsteps ], ax.get_ylim(),
                 linestyle=burnlinestyle, color=burnlinecolor )

        fig.canvas.draw()

# ======================================================================

class AutoCorPlot( PlotWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( explorer, *args, **kwargs )

        self.explorercheckwidget = self.explorer.showcorr
        self.setWindowTitle( "Autocorrelation" )

        subwindow = qt.QWidget()
        self.mainvbox.addWidget( subwindow, 0 )
        hbox = qt.QHBoxLayout()
        subwindow.setLayout( hbox )

        self.showchain = qt.QRadioButton( "chain", subwindow )
        hbox.addWidget( self.showchain, 0 )
        self.showburn = qt.QRadioButton( "burn chain", subwindow )
        hbox.addWidget( self.showburn, 0 )
        self.showfull = qt.QRadioButton( "full chain", subwindow )
        hbox.addWidget( self.showfull, 0 )
        self.showsteps = qt.QRadioButton( "steps", subwindow )
        hbox.addWidget( self.showsteps, 0 )
        self.startstep = qt.QLineEdit( "0" )
        hbox.addWidget( self.startstep, 1 )
        hbox.addWidget( qt.QLabel( "to" ), 0 )
        self.endstep = qt.QLineEdit( str( len(explorer.chain) // explorer.walkers ) )
        hbox.addWidget( self.endstep , 1 )
        hbox.addWidget( qt.QLabel( "Walker #" ), 0 )
        self.whichwalker = qt.QLineEdit( "0" )
        self.whichwalker.setValidator( qtgui.QIntValidator( 0, self.explorer.walkers, subwindow ) )
        hbox.addWidget( self.whichwalker, 1 )
        button = qt.QPushButton( "+" )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.incrementwalker )
        button = qt.QPushButton( "−" )
        hbox.addWidget( button, 0 )
        button.clicked.connect( self.decrementwalker )


        self.showfull.setChecked( True )

        self.update_plot()

        for toggleconnect in ( self.showchain, self.showburn, self.showfull, self.showsteps ):
            toggleconnect.toggled.connect( self.update_plot )
        for editedconnect in ( self.startstep, self.endstep, self.whichwalker ):
            editedconnect.editingFinished.connect( self.update_plot )

        self.explorer.addParamListener( self.update_plot )

    def incrementwalker( self, ev ):
        cur = int( self.whichwalker.text() )
        cur += 1
        if cur < 0: cur = 0
        if cur > self.explorer.walkers: cur = self.explorer.walkers - 1
        self.whichwalker.setText( str( cur ) )
        self.update_plot()

    def decrementwalker( self, ev ):
        cur = int( self.whichwalker.text() )
        cur -= 1
        if cur < 0: cur = 0
        if cur > self.explorer.walkers: cur = self.explorer.walkers - 1
        self.whichwalker.setText( str( cur ) )
        self.update_plot()

    def update_plot( self ):
        fig = self.canvas.figure
        fig.clf()

        linestyle = "solid"
        linecolor = "royalblue"
        zerostyle = "dashed"
        zerocolor = "maroon"
        titlesize = 16
        axistitlesize = 12
        labelsize = 10

        whichwalker = int( self.whichwalker.text() )

        firststep = 0
        laststep = self.explorer.burnsteps + self.explorer.steps
        if ( self.showchain.isChecked() ):
            firststep = self.explorer.burnsteps
            laststep = self.explorer.burnsteps + self.explorer.steps
        elif ( self.showburn.isChecked() ):
            firststep = 0
            laststep = self.explorer.burnsteps
        elif ( self.showsteps.isChecked() ):
            firststep = int( self.startstep.text() )
            laststep = int( self.endstep.text() )
            if ( firststep < 0 ): firststep = 0
            if ( firststep >= self.explorer.steps ): firststep = self.explorer.steps - 1
            if ( laststep <= firststep ): laststep = firststep + 1
            if ( laststep > self.explorer.steps ): self.explorer.steps
            firststep *= self.explorer.walkers + whichwalker
            laststep *= self.explorer.walkers + whichwalker

        firstoff = firststep * self.explorer.walkers + whichwalker
        lastoff = laststep * self.explorer.walkers + whichwalker
        lastoff = min( lastoff, len( self.explorer.chain ) )

        fulldf = self.explorer.chain
        steps = range( firststep, laststep )
        df = self.explorer.chain[ firstoff:lastoff:self.explorer.walkers ]

        sys.stderr.write( f"...plotting correlations for steps {firststep} to {laststep}, "
                          f"offset {firstoff} to {lastoff}, len(df)={len(df)}...\n" )

        columns = [ df.columns[i.row()] for i in self.explorer.paramswid.selectedIndexes() ]

        ncols = int( numpy.sqrt( len(columns) ) + 0.5 )
        nrows = int( numpy.ceil( len(columns) / ncols ) )
        row = 1
        col = 1
        dex = 1

        for param in columns:
            tmp = df[param].values - df[param].values.mean()
            ac = autocor( tmp )
            ax = fig.add_subplot( nrows, ncols, dex )
            ax.plot( numpy.arange( len(ac) ), ac, linestyle=linestyle, color=linecolor )


            ax.tick_params( axis='both', labelsize=labelsize )
            ax.set_xlabel( 'Step separation', fontsize=axistitlesize )
            ax.set_ylabel( 'Autocorrelation', fontsize=axistitlesize )
            ax.plot( ax.get_xlim(), [0, 0], linestyle=zerostyle, color=zerocolor )
            lab = self.titles[ param ] if param in self.titles else f'${param}$'
            ax.text( ax.get_xlim()[1], ax.get_ylim()[1], lab, fontsize=titlesize,
                     horizontalalignment='right', verticalalignment='top' )

            dex += 1
            col += 1
            if ( col > ncols ):
                col = 1
                row += 1

        fig.canvas.draw()


# ======================================================================
# I'm not convinced this is done right; look into it!

class ACLengthEstimatePlot( PlotWindow ):
    def __init__( self, explorer, *args, **kwargs ):
        super().__init__( explorer, *args, **kwargs )

        self.explorercheckwidget = self.explorer.showcorrtvsl
        self.setWindowTitle( "Autocorrelation" )

        self.explorer.addParamListener( self.update_plot )

    def update_plot( self ):
        sys.stderr.write( "...ploting autocorrelation time vs. Δt...\n" )
        fig = self.canvas.figure
        fig.clf()

        linestyle = "solid"
        linecolor = "royalblue"
        zerostyle = "dashed"
        zerocolor = "maroon"
        pointstyle = "o"
        pointcolor = "royalblue"
        titlesize = 16
        axistitlesize = 12
        labelsize = 10

        df = self.explorer.chain
        dfs = []
        for i in range( self.explorer.walkers ):
            dfs.append( df[i::self.explorer.walkers] )

        columns = [ df.columns[i.row()] for i in self.explorer.paramswid.selectedIndexes() ]
        ncols = int( numpy.sqrt( len(columns) ) + 0.5 )
        nrows = int( numpy.ceil( len(columns) / ncols ) )
        row = 1
        col = 1
        dex = 1

        # Based on https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
        N = numpy.exp( numpy.linspace( numpy.log(100), numpy.log( len(df) ), 10 ) ).astype( int )
        for param in columns:
            acbar = numpy.zeros( len(dfs[0]) )
            for tmpdf in dfs:
                tmp = tmpdf[param].values - tmpdf[param].values.mean()
                acbar += autocor( tmp )
            acbar /= len( dfs )

            # ax = fig.add_subplot( nrows, ncols, dex )
            # ax.set_title( f"Autocor for {param}", fontsize=titlesize )
            # ax.plot( numpy.arange( len(acbar) ), acbar, linestyle=linestyle, color=linecolor )
            # ax.plot( ax.get_xlim(), [0, 0], linestyle=zerostyle, color=zerocolor )
            # fig.show()

            y = []
            for n in N:
                taus = 2.0 * numpy.cumsum( acbar[:n] ) - 1.
                y.append( taus[ auto_window( taus, 5.0 ) ] )

            print( f"Autocorrelation time for {param}: {y[-1]}" )

            ax = fig.add_subplot( nrows, ncols, dex )
            ax.set_xscale( 'log' )
            ax.set_yscale( 'log' )
            ax.set_xlabel( "N" )
            ax.set_ylabel( r"$t_{ac}$" )
            ax.set_title( param )
            ax.plot( N, y, marker='o' )

            dex += 1
            col += 1
            if col > ncols:
                col = 1
                row += 1

        fig.canvas.draw()



# ======================================================================

class ChainExplorer(qt.QMainWindow):
    def __init__( self, app, filename, *args, **kwargs ):
        super().__init__( *args, **kwargs )

        self.read_debug_chain_file( filename )

        self.setWindowTitle( "pvdesnfit chain explorer" )

        self.cornerplot = None
        self.paramvsstepplot = None
        self.acceptvstplot = None
        self.corrplot = None
        self.corrtvslplot = None

        self.paramlisteners = set()

        self.app = app
        self.font = self.app.font()
        self.titlefont = qtgui.QFont( self.font.family(), int( 1.5*self.font.pointSize() ), qtgui.QFont.Bold )
        self.boldfont = qtgui.QFont( self.font.family(), self.font.pointSize(), qtgui.QFont.Bold )

        self.mainwidget = qt.QWidget()
        self.setCentralWidget( self.mainwidget )
        mainhbox = qt.QHBoxLayout()
        self.mainwidget.setLayout( mainhbox )

        # Left column: parameters

        subwindow = qt.QWidget()
        mainhbox.addWidget( subwindow, 0 )
        vbox = qt.QVBoxLayout()
        subwindow.setLayout( vbox )

        label = qt.QLabel( "Parameters:" )
        label.setFont( self.titlefont )
        vbox.addWidget( label, 0 )
        paramswrapper = ListWrapper( list( self.chain.columns ) )
        self.paramswid = qt.QListView()
        self.paramswid.setModel( paramswrapper )
        self.paramswid.setSelectionMode( qt.QAbstractItemView.SelectionMode.MultiSelection )
        for i in range( 0, len( self.params ) ):
            self.paramswid.setCurrentIndex( paramswrapper.index( i, 0 ) )
        vbox.addWidget( self.paramswid, 1 )

        self.paramswid.selectionModel().selectionChanged.connect( self.notifyParamListeners )

        # Middle column: buttons

        subwindow = qt.QWidget()
        mainhbox.addWidget( subwindow, 0 )
        vbox = qt.QVBoxLayout()
        subwindow.setLayout( vbox )

        def winshowhide( widget, prop, cls ):
            if widget.isChecked():
                if getattr( self, prop ) is None:
                    obj = cls( self )
                    sys.stderr.write( f"cls={cls}, obj={obj}\n" )
                    setattr( self, prop, obj )
                getattr( self, prop ).show()
            else:
                getattr( self, prop ).hide()


        self.showcorner = qt.QCheckBox( "Show Corner Plot" )
        self.showcorner.setChecked( False )
        vbox.addWidget( self.showcorner, 0 )
        self.showcorner.toggled.connect( lambda ev : winshowhide( self.showcorner, 'cornerplot', CornerPlot ) )

        self.showparamvsstep = qt.QCheckBox( "Show Param vs. Step" )
        self.showparamvsstep.setChecked( False )
        vbox.addWidget( self.showparamvsstep, 0 )
        self.showparamvsstep.toggled.connect( lambda ev : winshowhide( self.showparamvsstep, 'paramvsstepplot',
                                                                       ParamVsStepPlot ) )

        self.showacceptvst = qt.QCheckBox( "Show Acceptance Fraction vs. Step" )
        self.showacceptvst.setChecked( False )
        vbox.addWidget( self.showacceptvst )
        self.showacceptvst.toggled.connect( lambda ev: winshowhide( self.showacceptvst, 'acceptvstplot',
                                                                    AcceptanceVsStepPlot ) )

        self.showcorr = qt.QCheckBox( "Show Autocorrelations" )
        self.showcorr.setChecked( False )
        vbox.addWidget( self.showcorr, 0 )
        self.showcorr.toggled.connect( lambda ev: winshowhide( self.showcorr, 'corrplot', AutoCorPlot ) )

        # Add this back when you believe ACLengthEstimatePlot
        # self.showcorrtvsl = qt.QCheckBox( "Show AC Length Estimate vs. Length" )
        # self.showcorrtvsl.setChecked( False )
        # vbox.addWidget( self.showcorrtvsl, 0 )
        # self.showcorrtvsl.toggled.connect( lambda ev: winshowhide( self.showcorrtvsl, 'corrtvslplot',
        #                                                            ACLengthEstimatePlot ) )

        vbox.addStretch( 1 )

        # Right column: info

        subwindow = qt.QWidget()
        mainhbox.addWidget( subwindow, 0 )
        vbox = qt.QVBoxLayout()
        subwindow.setLayout( vbox )

        for att in [ 'globalmpisize', 'mpisize', 'submpisize', 'ompsize', 'allparams', 'params',
                     'walkers', 'burnsteps', 'steps', 'stretchparam' ]:
            subwindow = qt.QWidget()
            vbox.addWidget( subwindow, 0 )
            hbox = qt.QHBoxLayout()
            hbox.setContentsMargins( 11, 0, 11, 0 )
            subwindow.setLayout( hbox )
            label = qt.QLabel( att )
            label.setFont( self.boldfont )
            hbox.addWidget( label, 0 )
            label = qt.QLabel( str( getattr( self, att ) ) )
            hbox.addWidget( label, 0 )

        vbox.addStretch( 1 )


    def read_debug_chain_file( self, filename ):
        mpire = re.compile(  "^\s*#\s*globalmpisize\s*=\s*(?P<globalmpisize>\d+)\s*,\s*"
                             "mpisize\s*=\s*(?P<mpisize>\d+)\s*,\s*"
                             "submpisize\s*=\s*(?P<submpisize>\d+)\s*,\s*"
                             "OMP size\s*=\s*(?P<ompsize>\d+)\s*$" )
        paramre = re.compile( "^\s*#\s*parameters\s*:\s*(?P<params>.*)\s*$" )
        walkerre = re.compile( "^\s*#\s*(?P<walkers>\d+) walkers, (?P<burnsteps>\d+) burn-in steps, "
                               "(?P<steps>\d+)\s*steps; stretchparam\s*=\s*(?P<stretchparam>[\-\d\.eE]+)" )
        fixedre = re.compile( "\[fixed\]$" )

        with open( filename ) as ifp:
            for line in ifp:
                if ( line[0] != '#' ) and ( line != '^\s*$' ):
                    break
                match = mpire.search( line )
                if match is not None:
                    self.globalmpisize = int( match.group('globalmpisize' ) )
                    self.mpisize = int( match.group('mpisize') )
                    self.submpisize = int( match.group('submpisize') )
                    self.ompsize = int( match.group("ompsize") )
                    continue
                match = paramre.search( line )
                if match is not None:
                    self.allparams = match.group('params').split()
                    self.params = [ i for i in self.allparams if not fixedre.search( i ) ]
                    continue
                match = walkerre.search( line )
                if match is not None:
                    self.walkers = int( match.group('walkers') )
                    self.burnsteps = int( match.group('burnsteps') )
                    self.steps = int( match.group( 'steps' ) )
                    self.stretchparam = float( match.group( 'stretchparam' ) )
                    continue

        atts = [ 'globalmpisize', 'mpisize', 'submpisize', 'ompsize', 'allparams', 'params',
                 'walkers', 'burnsteps', 'steps', 'stretchparam' ]
        missing = set()
        for att in atts:
            if not hasattr( self, att ):
                missing.add( att )

        if len(missing) > 0:
            raise RuntimeError( f"Failed to parse some parameters from comments in debug chain file: {missing}" )

        self.chain = pandas.read_csv( filename, delim_whitespace=True, comment='#' )

        ifp.close()

    def addParamListener( self, fun ):
        self.paramlisteners.add( fun )

    def removeParamListener( self, fun ):
        if fun in self.paramlisteners:
            self.paramlisteners.remove( fun )

    def notifyParamListeners( self ):
        for fun in self.paramlisteners:
            fun()

    def closeEvent( self, ev ):
        sys.stderr.write( "Closing\n" )
        # This isn't elegant, but it gets the job done
        sys.exit(0)


def main():
    parser = argparse.ArgumentParser( 'explorechain', 'Do things to a pvdensfit debug chain' )
    parser.add_argument( 'debugchainname', help="Name of debug chain file" )
    args = parser.parse_args()

    matplotlib.rc( 'mathtext', fontset='cm' )
    app = qt.QApplication( [] )
    explorer = ChainExplorer( app, args.debugchainname )
    explorer.show()
    app.exec_()


# ======================================================================

if __name__ == "__main__":
    main()
