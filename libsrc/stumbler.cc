#include <iostream>
#include <stumbler.hh>
#include <cmath>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <type_traits>
#include <limits>
#include <unistd.h>
#include <omp.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <gsl/gsl_randist.h>

// TODO : spread seed out into different seeds for each rank in comm

template<typename dtype>
Stumbler<dtype>::Stumbler( int nwalkers,
                           int burnin,
                           int nsteps,
                           dtype stretchparam,
                           const std::vector<dtype> &initparam,
                           const std::vector<dtype> &initsigma,
                           const std::vector<std::string> &paramnames,
                           std::function<dtype(const std::vector<dtype>&, const MPI_Comm&)> loglikelihood,
                           MPI_Comm &comm,
                           MPI_Comm &stumblercomm,
                           MPI_Comm &subcomm,
                           const std::string &chainname,
                           const std::string &burnchainname,
                           const std::string &debugchainname,
                           bool outhdf5,
                           size_t dumpchainevery,
                           unsigned long int seed,
                           std::shared_ptr<spdlog::logger> logger)
: nwalkers(nwalkers),
  burnin(burnin),
  nsteps(nsteps),
  stretchparam(stretchparam),
  initparam(initparam),
  initsigma(initsigma),
  paramnames(paramnames),
  loglikelihood(loglikelihood),
  chainname(chainname),
  burnchainname(burnchainname),
  debugchainname(debugchainname),
  comm(comm),
  stumblercomm(stumblercomm),
  subcomm(subcomm),
  outhdf5(outhdf5),
  dumpchainevery(dumpchainevery),
  rngseed(seed),
  debugdump(nullptr),
  do_not_calculate_covar(false)
{
  if ( logger != nullptr ) {
    this->logger = logger;
  }
  else {
    this->logger = spdlog::get( "Stumbler" );
    if ( this->logger == nullptr ) {
      this->logger = spdlog::stderr_color_mt( "Stumbler" );
      this->logger->set_level( spdlog::level::info );
    }
  }

  if ( std::is_same<dtype,double>::value ) {
    mpitype = MPI_DOUBLE;
    hdf5type = H5T_NATIVE_DOUBLE;
  }
  else if ( std::is_same<dtype,float>::value ) {
    mpitype = MPI_FLOAT;
    hdf5type = H5T_NATIVE_FLOAT;
  }
  else
    throw std::runtime_error( "Unknown datatype!  Stumbler only supports float and double." );

  if ( nwalkers <= initparam.size() )
    throw std::runtime_error( "Must have more walkers than parameters" );
  if ( nwalkers % 2 != 0 )
    throw std::runtime_error( "Number of walkers must be even" );

  if ( outhdf5 && ( burnin % dumpchainevery != 0 ) )
    throw std::runtime_error( "If writing to hdf5 files, burnin must be an even multiple of dumpchainevery" );

  MPI_Comm_rank( comm, &globalmpirank );
  MPI_Comm_size( comm, &globalmpisize );
  if ( stumblercomm != MPI_COMM_NULL ) {
    MPI_Comm_rank( stumblercomm, &mpirank );
    MPI_Comm_size( stumblercomm, &mpisize );
  } else {
    mpirank = -1;
    mpisize = -1;
  }
  MPI_Comm_rank( subcomm, &submpirank );
  MPI_Comm_size( subcomm, &submpisize );

  // Every rank is going to need to know mpisize
  MPI_Bcast( &mpisize, 1, MPI_INT, 0, comm );

  if ( true ) {
    std::stringstream str("");
    str << "MPI ranks: global=" << globalmpirank << ", stumbler=" << mpirank << ", sub=" << submpirank;
    this->logger->debug( str.str() );
  }


  // ****
  // std::stringstream str("");
  // str << "World rank " << worldmpirank << " has global rank " << globalmpirank
  //     << ", rank " << mpirank << ", and subrank " << submpirank;
  // this->logger->warn( str.str() );
  // ****

  nparam = initparam.size();

  fixparam.resize( nparam );
  for ( auto i=fixparam.begin() ; i!=fixparam.end() ; ++i ) *i = false;

  keepdebuginfo = ( debugchainname.size() > 0 );
  additional_debug_headers.clear();

  newparam.resize(nwalkers);
  param.resize(nwalkers);
  lnL.resize(nwalkers);
  newlnL.resize(nwalkers);
  step_accepted.resize(nwalkers);
  others.resize(nwalkers);
  zs.resize(nwalkers);
  // This is a bit gratuitous, but whatevs
  walkers.resize(nwalkers);
  for ( short int i = 0 ; i < nwalkers ; ++i ) walkers[i] = i;

  for ( int i=0 ; i<nwalkers ; ++i ) {
    param[i].resize( nparam );
    newparam[i].resize( nparam );
  }

  if ( burnin < 0 ) this->burnin = 0;

  if ( submpirank == 0 ) {
    rng = gsl_rng_alloc( gsl_rng_ranlxs0 );
  }

  // See step(); z0=1/a, z1=a, a = stretchparam
  // x = 2√z
  stretchx0 = 2. * sqrt( 1 / stretchparam );
  stretchxr = 2. * sqrt( stretchparam ) - stretchx0;

  dtypewidechains.clear();
  dtypechains.clear();
  shortchains.clear();

  for ( auto ci = dtypewidenames.begin() ; ci != dtypewidenames.end() ; ++ci )
    dtypewidechains[*ci] = std::make_unique<StumblerChain<dtype>>( dumpchainevery, nwalkers, nparam );
  for ( auto ci = dtypenames.begin() ; ci != dtypenames.end() ; ++ci )
    dtypechains[*ci] = std::make_unique<StumblerChain<dtype>>( dumpchainevery, nwalkers, 1 );
  for ( auto ci = shortnames.begin() ; ci != shortnames.end() ; ++ci )
    shortchains[*ci] = std::make_unique<StumblerChain<short int>>( dumpchainevery, nwalkers, 1 );

}

template<typename dtype>
Stumbler<dtype>::~Stumbler() {
  if ( submpirank == 0 ) {
    gsl_rng_free( rng );
  }
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::logconfig() {
  std::stringstream str("");
  str << "Stumbler has " << nwalkers << " walkers, burnin=" << burnin
      << ", nsteps=" << nsteps << ", stretchparam=" << stretchparam << std::endl;
  str << "    Random seed=" << rngseed << std::endl;
  if ( anneal ) str << "    Will do stimulated annealing with initial β= " << initbeta << std::endl;
  else str << "    Will NOT do stimulated annealing" << std::endl;
  str << "    Will " << (cluster ? "" : "NOT " ) << "recluster 3/4 through burn-in." << std::endl;
  if ( rutrecluster ) str << "    Will recluster when in a rut of " << rutlength << " steps." << std::endl;
  else str << "    Will not recluster when in a rut." << std::endl;
  str << "    Will " << (keepdebuginfo ? "" : "NOT " ) << "keep debug info." << std::endl;
  logger->info( str.str() );
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::set_param_fixed( const std::vector<bool> &fixed ) {
  if ( fixed.size() != initparam.size() )
    throw std::runtime_error( "Length of vector passed to set_param_fixed must match number of parametrs" );
  for ( auto i=0 ; i < fixed.size() ; ++i )
    fixparam[i] = fixed[i];
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::set_anneal( bool b ) {
  anneal = b;
  if ( burnin == 0 ) {
    logger->warn( "Ignoring anneal since burnin is 0" );
    anneal = false;
  }
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::set_debugdump( std::function<void()> fun ) {
  debugdump = fun;
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::initialize_parameters() {
  for ( int i = 0 ; i < nwalkers ; ++i ) {
    if ( globalmpirank == 0 ) {
      for ( int j = 0 ; j < initparam.size() ; ++j ) {
        if ( fixparam[j] )
          param[i][j] = initparam[j];
        else
          param[i][j] = initparam[j] + gsl_ran_gaussian( rng, initsigma[j] );
      }
    }
    MPI_Bcast( param[i].data(), nparam, mpitype, 0, comm );
    lnL[i] = beta * loglikelihood( param[i], subcomm );
  }
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::create_chain_files() {
  if ( outhdf5 )
    create_chain_files_hdf5();
  else
    create_chain_files_txt();
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::create_chain_files_txt() {
  if ( chainname.size() > 0 ) {
    create_one_chain_file_txt( chainname, false );
  }
  if ( burnchainname.size() > 0 ) {
    create_one_chain_file_txt( burnchainname, false );
  }
  if ( debugchainname.size() > 0 ) {
    create_one_chain_file_txt( debugchainname, true );
  }
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::create_one_chain_file_txt( const std::string &filename, bool debug )
{
  std::ofstream ofp;

  ofp.open( filename, std::ios_base::out );
  if ( debug ) {
    char hostname[80];
    gethostname( hostname, 80 );
    const auto now = std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() );
    ofp << "# Stumbler run at " << std::put_time( std::gmtime( &now ), "%F %T" ) << " UTC on "
        << hostname << std::endl;
    ofp << "# globalmpisize=" << globalmpisize << ", mpisize=" << mpisize
        << ", submpisize=" << submpisize << ", OMP size=" << omp_get_max_threads() << std::endl;
    ofp << "# parameters : ";
    auto fi = fixparam.begin();
    for ( auto pi = paramnames.begin() ; pi != paramnames.end() ; ++pi, ++fi ) {
      ofp << *pi << ( *fi ? "[fixed]" : "" );
      if ( std::next(pi) != paramnames.end() ) ofp << " ";
    }
    ofp << std::endl;
    ofp << "# " << nwalkers << " walkers, " << burnin << " burn-in steps, "
        << nsteps << " steps; stretchparam=" << stretchparam << std::endl;
    ofp << "# seed = " << rngseed << std::endl;
    for ( auto si = additional_debug_headers.begin() ; si != additional_debug_headers.end() ; ++si ) {
      ofp << "# " << *si << std::endl;
    }
  }

  for ( int i = 0 ; i < paramnames.size() ; ++i ) {
    if ( !fixparam[i] ) {
      ofp << paramnames[i] << " ";
    }
  }
  if ( debug ) {
    for ( int i = 0 ; i < paramnames.size() ; ++i ) {
      if ( fixparam[i] ) {
        ofp << paramnames[i] << " ";
      }
    }
    ofp << "walker other z accepted lnL prop_lnL ";
    for ( int i = 0 ; i < paramnames.size() ; ++i ) {
      if ( !fixparam[i] ) {
        ofp << "prop_" << paramnames[i] << " ";
      }
    }
  }
  ofp << std::endl;
  ofp.close();
}


// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::create_chain_files_hdf5() {
  if ( chainname.size() > 0 ) {
    create_one_chain_file_hdf5( chainname, false );
  }
  if ( burnchainname.size() > 0 ) {
    create_one_chain_file_hdf5( burnchainname, false );
  }
  if ( debugchainname.size() > 0 ) {
    create_one_chain_file_hdf5( debugchainname, true );
  }
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::create_one_chain_file_hdf5( const std::string &filename, bool debug )
{
  // I really want to use the HDF5 C++ interface, but the documentation is
  // basically nonexistent.  (The API is listed, with no description.)
  std::stringstream str("");
  str << "Creating HDF5 file " << filename;
  logger->info( str.str() );

  hid_t h5file = H5Fcreate( filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
  if  ( h5file == H5I_INVALID_HID ) {
    str.str("");
    str << "Failed to create file " << filename;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  hsize_t dims[2]{ 0, (hsize_t)nparam };
  hsize_t maxdims[2]{ H5S_UNLIMITED, (hsize_t)nparam };
  hid_t space = H5Screate_simple( 2, dims, maxdims );
  hid_t plist = H5Pcreate( H5P_DATASET_CREATE );
  H5Pset_layout( plist, H5D_CHUNKED );
  hsize_t chunk_dims[2]{ (hsize_t)dumpchainevery * (hsize_t)nwalkers, (hsize_t)nparam };
  H5Pset_chunk( plist, 2, chunk_dims );
  hid_t dset = H5Dcreate( h5file, "chain", hdf5type, space, H5P_DEFAULT, plist, H5P_DEFAULT );

  if ( debug ) {
    hid_t space = H5Screate_simple( 2, dims, maxdims );
    hid_t plist = H5Pcreate( H5P_DATASET_CREATE );
    H5Pset_layout( plist, H5D_CHUNKED );
    H5Pset_chunk( plist, 2, chunk_dims );
    hid_t dset = H5Dcreate( h5file, "proposed", hdf5type, space, H5P_DEFAULT, plist, H5P_DEFAULT );

    H5Dclose( dset );
    H5Pclose( plist );
    H5Sclose( space );

    hsize_t dims1d[1]{ 0 };
    hsize_t maxdims1d[1]{ H5S_UNLIMITED };

    for ( auto di = debugparams.begin() ; di != debugparams.end() ; ++di ) {
      hid_t space = H5Screate_simple( 1, dims1d, maxdims1d );
      hid_t plist = H5Pcreate( H5P_DATASET_CREATE );
      H5Pset_layout( plist, H5D_CHUNKED );
      hsize_t chunk_dims1d[1]{ (hsize_t)dumpchainevery * (hsize_t)nwalkers };
      H5Pset_chunk( plist, 1, chunk_dims1d );
      hid_t dset;
      if ( dtypechains.find(*di) == dtypechains.end() ) {
        dset = H5Dcreate( h5file, di->c_str(), H5T_NATIVE_SHORT, space, H5P_DEFAULT, plist, H5P_DEFAULT );
      } else {
        dset = H5Dcreate( h5file, di->c_str(), hdf5type, space, H5P_DEFAULT, plist, H5P_DEFAULT );
      }

      H5Dclose( dset );
      H5Pclose( plist );
      H5Sclose( space );
    }
  }

  // Parameters attribute.
  hid_t stringtype = H5Tcopy(H5T_C_S1);
  H5Tset_size( stringtype, H5T_VARIABLE );
  hsize_t h5nparam = nparam;
  hid_t attrspace = H5Screate_simple( 1, &h5nparam, nullptr );
  hid_t attr = H5Acreate( dset, "parameters", stringtype, attrspace, H5P_DEFAULT, H5P_DEFAULT );
  char **buffer = new char*[nparam];
  for ( int i = 0 ; i < nparam ; ++i) { buffer[i] = paramnames[i].data(); }
  H5Awrite( attr, stringtype, buffer );
  H5Aclose( attr );
  H5Sclose( attrspace );
  delete[] buffer;
  H5Tclose( stringtype );

  // nwalkers attribute
  hsize_t one = 1;
  attrspace = H5Screate_simple( 1, &one, nullptr );
  attr = H5Acreate( dset, "nwalkers", H5T_NATIVE_INT, attrspace, H5P_DEFAULT, H5P_DEFAULT );
  H5Awrite( attr, H5T_NATIVE_INT, &nwalkers );
  H5Aclose( attr );

  // burnin attribute
  attr = H5Acreate( dset, "burnin", H5T_NATIVE_INT, attrspace, H5P_DEFAULT, H5P_DEFAULT );
  H5Awrite( attr, H5T_NATIVE_INT, &burnin );
  H5Aclose( attr );

  // stretchparam attribute
  attr = H5Acreate( dset, "stretchparam", hdf5type, attrspace, H5P_DEFAULT, H5P_DEFAULT );
  H5Awrite( attr, hdf5type, &stretchparam );
  H5Aclose( attr );
  H5Sclose( attrspace );

  // for ( int i = 0 ; i < nparam ; ++i ) {
  //   std::stringstream paramnamestr("");
  //   paramnamestr << "param" << i;
  //   std::string paramname = paramnamestr.str();
  //   hsize_t one = 1;
  //   hid_t attrspace = H5Screate_simple( 1, &one, nullptr );
  //   hid_t attr = H5Acreate( dset, paramname.c_str(), stringtype, attrspace, H5P_DEFAULT, H5P_DEFAULT );
  //   char **buffer = new char*[1];
  //   buffer[0] = new char[ paramnames[i].size() + 1 ];
  //   memcpy( buffer[0], paramnames[i].data(), paramnames[i].size()+1 );
  //   H5Awrite( attr, stringtype, buffer );
  //   H5Aclose( attr );
  //   H5Sclose( attrspace );
  //   delete[] buffer[0];
  //   delete[] buffer;
  // }
  // H5Tclose( stringtype );

  H5Dclose( dset );
  H5Pclose( plist );
  H5Sclose( space );
  H5Fclose( h5file );
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::reinitialize( unsigned long int seed ) {
  if ( globalmpirank == 0 ) {
    for ( auto ci = dtypewidechains.begin() ; ci != dtypewidechains.end() ; ++ci )
      ci->second->cur = 0;
    for ( auto ci = dtypechains.begin() ; ci != dtypechains.end() ; ++ci )
      ci->second->cur = 0;
    for ( auto ci = shortchains.begin() ; ci != shortchains.end() ; ++ci )
      ci->second->cur = 0;
  }

  if ( submpirank == 0 ) {
    std::stringstream str("");
    if ( mpisize > 1 ) {
      // Need to make sure each rank gets a different seed!
      auto seedrng = gsl_rng_alloc( gsl_rng_ranlxs0 );
      gsl_rng_set( seedrng, seed );
      for ( int i = 0 ; i < mpirank ; ++i ) {
        seed = gsl_rng_get( seedrng );
      }
      gsl_rng_free( seedrng );
    }
    str << "Rank " << mpirank << " (global " << globalmpirank << ") initializing rng with seed " << seed;
    logger->debug( str.str() );
    gsl_rng_set( rng, seed );
  }
  curstep = 0;
  wlkrsteps = 0;
  naccepted = 0;
  naccepted_at_burnin = 0;

  reclustered = false;
  if ( ( burnin > 0 ) & ( anneal ) ) {
    beta = initbeta;
    logbeta = log( beta );
    logbetastep = -logbeta / ( burnin/2 );
  } else {
    beta = 1.;
    logbeta = 0.;
    logbetastep = 0.;
  }

  initialize_parameters();

  // ****
  if ( globalmpirank == 0 ) {
    std::stringstream str("");
    for ( int i = 0 ; i < nwalkers ; ++i ) {
      str.str("");
      str << "Walker " << i << ": param = ";
      for ( int j = 0 ; j < initparam.size() ; ++j ) {
        str << param[i][j] << " ";
      }
      str << " ; β=" << beta << ", β*lnL = " << lnL[i];
      logger->debug( str.str() );
    }
  }
  // ****

  // Initialize mean and covar storage
  if ( globalmpirank == 0 && ( ! do_not_calculate_covar ) ) {
    working_mean.resize( initparam.size() );
    working_covar.resize( initparam.size() );
    for ( int i = 0 ; i < initparam.size() ; ++i ) {
      working_mean[i] = 0.;
      working_covar[i].resize( initparam.size() );
      for ( int j = 0 ; j < initparam.size() ; ++j ) working_covar[i][j] = 0.;
    }
  }

  if ( globalmpirank == 0 ) {
    if ( chainname.size() == 0 )
      logger->warn( "Not writing output chain, no chain filename given." );
    create_chain_files();
  }
}

// **********************************************************************
// Only call this from the mpi rank that will actually do anything

template<typename dtype>
void Stumbler<dtype>::dump_chains_to_txt_file( const std::string &filename, bool debug )
{
  if ( globalmpirank != 0 ) return;
  if ( dtypewidechains["chain"]->cur == 0 ) return;

  std::ofstream ofp;

  ofp.open( filename, std::ios_base::app );
  for ( int i = 0 ; i < dtypewidechains["chain"]->cur * nwalkers ; ++i ) {
    for ( int j = 0 ; j < nparam ; ++j ) {
      if ( !fixparam[j] ) {
        ofp << dtypewidechains["chain"]->vals[i * nparam + j] << " ";
      }
    }
    if ( debug ) {
      for ( int j = 0 ; j < nparam ; ++j ) {
        if ( fixparam[j] ) {
          ofp << dtypewidechains["chain"]->vals[i * nparam + j] << " ";
        }
      }
      ofp << shortchains["walker"]->vals[i] << " "
          << shortchains["other"]->vals[i] << " "
          << dtypechains["z"]->vals[i] << " "
          << shortchains["accepted"]->vals[i] << " "
          << dtypechains["lnL"]->vals[i] << " "
          << dtypechains["proposed_lnL"]->vals[i] << " ";
      for ( int j = 0 ; j < nparam ; ++j ) {
        if ( ! fixparam[j] ) {
          ofp << dtypewidechains["proposed"]->vals[i * nparam + j] << " ";
        }
      }
    }
    ofp << std::endl;
  }
  ofp.close();
}

// **********************************************************************
// Only call this from the mpi rank that will actually do anything

template<typename dtype>
void Stumbler<dtype>::dump_chains_to_hdf5_file( const std::string &filename, bool debug )
{
  if ( globalmpirank != 0 ) return;

  std::stringstream str("");
  hid_t h5file = H5Fopen( filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );

  hid_t dset = H5Dopen2( h5file, "chain", H5P_DEFAULT );
  hid_t space = H5Dget_space( dset );
  hsize_t ndims = H5Sget_simple_extent_ndims( space );
  if ( ndims != 2 ) {
    str.str("");
    str << "ndims is " << ndims << ", should be 2.";
    logger->error( str.str() );
    H5Sclose( space );
    H5Dclose( dset );
    H5Fclose( h5file );
    throw std::runtime_error( str.str() );
  }
  hsize_t dims[2];
  hsize_t maxdims[2];
  H5Sget_simple_extent_dims( space, dims, maxdims );
  if ( dims[1] != nparam ) {
    str.str("");
    str << "dims[1] is " << dims[1] << ", should be nparam=" << nparam;
    logger->error( str.str() );
    H5Sclose( space );
    H5Dclose( dset );
    H5Fclose( h5file );
    throw std::runtime_error( str.str() );
  }
  hsize_t newdims[2]{ dims[0] + dtypewidechains["chain"]->cur * nwalkers, (hsize_t)nparam };
  H5Sclose( space );
  H5Dset_extent( dset, newdims );
  space = H5Dget_space( dset );
  hsize_t addedsize[2]{ (hsize_t)(dtypewidechains["chain"]->cur * nwalkers), (hsize_t)nparam };
  hsize_t slabstart[2]{ dims[0], 0 };
  H5Sselect_hyperslab( space, H5S_SELECT_SET, slabstart, nullptr, addedsize, nullptr );
  hid_t memspace = H5Screate_simple( 2, addedsize, nullptr );
  H5Dwrite( dset, hdf5type, memspace, space, H5P_DEFAULT, dtypewidechains["chain"]->vals.data() );

  H5Sclose( memspace );
  H5Sclose( space );
  H5Dclose( dset );

  if ( debug ) {
    dset = H5Dopen2( h5file, "proposed", H5P_DEFAULT );
    H5Dset_extent( dset, newdims );
    space = H5Dget_space( dset );
    H5Sselect_hyperslab( space, H5S_SELECT_SET, slabstart, nullptr, addedsize, nullptr );
    memspace = H5Screate_simple( 2, addedsize, nullptr );
    H5Dwrite( dset, hdf5type, memspace, space, H5P_DEFAULT, dtypewidechains["proposed"]->vals.data() );

    H5Sclose( memspace );
    H5Sclose( space );
    H5Dclose( dset );

    std::vector<dtype> dvals;
    dvals.resize( dtypewidechains["proposed"]->cur * nwalkers );
    std::vector<short int> ivals;
    ivals.resize( dtypewidechains["proposed"]->cur * nwalkers );

    for ( auto di = debugparams.begin() ; di != debugparams.end() ; ++di ) {
      hid_t dset = H5Dopen2( h5file, di->c_str(), H5P_DEFAULT );
      hid_t space = H5Dget_space( dset );
      hsize_t dims[1];
      hsize_t maxdims[1];
      H5Sget_simple_extent_dims( space, dims, maxdims );
      hsize_t newdims[1]{ dims[0] + dtypewidechains["chain"]->cur * nwalkers };
      H5Sclose( space );
      H5Dset_extent( dset, newdims );
      space = H5Dget_space( dset );
      hsize_t addedsize[1]{ (hsize_t)( dtypewidechains["chain"]->cur * nwalkers ) };
      hsize_t slabstart[1]{ dims[0] };
      H5Sselect_hyperslab( space, H5S_SELECT_SET, slabstart, nullptr, addedsize, nullptr );
      hid_t memspace = H5Screate_simple( 1, addedsize, nullptr );

      if ( dtypechains.find(*di) == dtypechains.end() ) {
        std::copy( shortchains[*di]->vals.begin(),
                   shortchains[*di]->vals.begin() + dtypewidechains["chain"]->cur * nwalkers,
                   ivals.begin() );
        H5Dwrite( dset, H5T_NATIVE_SHORT, memspace, space, H5P_DEFAULT, ivals.data() );
      }
      else {
        std::copy( dtypechains[*di]->vals.begin(),
                   dtypechains[*di]->vals.begin() + dtypewidechains["chain"]->cur * nwalkers,
                   dvals.begin() );
        H5Dwrite( dset, hdf5type, memspace, space, H5P_DEFAULT, dvals.data() );
      }

      H5Sclose( memspace );
      H5Sclose( space );
      H5Dclose( dset );
    }
  }

  H5Fclose( h5file );
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::dump_chains( int curstep )
{
  if ( globalmpirank != 0 ) return;

  // if ( dtypewidechains["chain"]->cur == 0 ) return;

  auto t0 = std::chrono::high_resolution_clock::now();
  std::stringstream str("");

  // logger->debug( "Dumping chains" );

  if ( keepdebuginfo ) {
    str.str("");
    str << "Adding " << dtypewidechains["chain"]->cur * nwalkers
        << " lines to " << debugchainname;
    logger->debug( str.str() );
    if ( outhdf5 )
      dump_chains_to_hdf5_file( debugchainname, true );
    else
      dump_chains_to_txt_file( debugchainname, true );
  }

  if ( curstep > burnin ) {
    str.str("");
    str << "Adding " << dtypewidechains["chain"]->cur * nwalkers
        << " lines to " << chainname;
    logger->debug( str.str() );
    if ( outhdf5 )
      dump_chains_to_hdf5_file( chainname, false );
    else
      dump_chains_to_txt_file( chainname, false );
  }
  else {
    str.str("");
    str << "Adding " << dtypewidechains["chain"]->cur * nwalkers
        << " lines to " << burnchainname;
    logger->debug( str.str() );
    if ( outhdf5 )
      dump_chains_to_hdf5_file( burnchainname, false );
    else
      dump_chains_to_txt_file( burnchainname, false );
  }

  dtypewidechains["chain"]->cur = 0;
  if ( keepdebuginfo ) {
    dtypewidechains["proposed"]->cur = 0;
    dtypechains["lnL"]->cur = 0;
    dtypechains["z"]->cur = 0;
    dtypechains["proposed_lnL"]->cur = 0;
    shortchains["walker"]->cur = 0;
    shortchains["other"]->cur = 0;
    shortchains["accepted"]->cur = 0;
  }
}

// **********************************************************************

/**
 * Try to find the best cluster of walkers that are in the global minimum.
 *
 * Do a DBSCAN clustering (see https://en.wikipedia.org/wiki/DBSCAN),
 * using the difference between lnL as the distance.
 *
 * Thinking of a likelihood like a chisquare, then a lnL difference
 * of 1 should correspond to something like a 1σ difference, so
 * call this the "epislon" difference for DBSCAN.
 *
 * Insist on a cluster size of nwalkers / 10 or 3, whichever is bigger.
 *
 * When done, choose the cluster with the lighest lnL, and then
 * calculate new initial parameters by looking at the mean and
 * sdev of the walkers in that cluster.
 */

template<typename dtype>
void Stumbler<dtype>::recluster() {
  std::stringstream str("");

  if ( globalmpirank == 0 ) {
    logger->debug( "Reclustering" );
    str.str("");
    str << "Walkers before cluster:" << std::endl;
    int oldprecision = str.precision( 3 );
    auto oldflags = str.flags( std::ios::scientific );
    for ( int i = 0 ; i < nwalkers ; ++i ) {
      str << "  " << std::setw(4) << i << ", lnL= " << lnL[i] << " : ";
      for ( int j = 0 ; j < nparam ; ++j ) {
        str << " " << param[i][j];
      }
      str << std::endl;
    }
    logger->debug( str.str() );

    const int clusterundef = -1;
    const int clusternoise = -2;
    dtype clusterrad = 1;
    int minclustersize = nwalkers / 10;
    if ( minclustersize < 3 ) minclustersize = 3;

    std::vector<int> label( nwalkers, clusterundef );
    std::vector<int> neighbors, subneighbors;
    std::vector<int> clustersize, clustersort, clusterorder;
    std::vector<dtype> clustermean, clustersdev;
    neighbors.reserve( nwalkers );
    subneighbors.reserve( nwalkers );

    // Measure distances between the parameters of all walkers, trusting initsigma as a scale
    // (Other option would be just lnL distance.)
    dtype mindist = std::numeric_limits<dtype>::max();
    std::vector<dtype> dist( nwalkers*nwalkers );
    for ( int i = 0 ; i < nwalkers ; ++i ) {
      for ( int j = i ; j < nwalkers ; ++j ) {
        dist[i * nwalkers + j] = 0.;
        for ( int k = 0 ; k < nparam ; ++k ) {
          dist[i * nwalkers + j] += ( ( ( param[i][k] - param[j][k] ) * ( param[i][k] - param[j][k] ) )
                                      / ( initsigma[k] * initsigma[k] ) );
        }
        dist[i * nwalkers + j] = sqrt( dist[i * nwalkers + j] );
        // dist[i][j] = fabs( lnL[i] - lnL[j] );
        dist[j * nwalkers + i] = dist[i * nwalkers + j];
      }
    }

    logger->debug( "Recluster: distances measured" );

    // Sort distances
    std::vector<int> distsort( nwalkers*nwalkers );
    for ( int i = 0 ; i < nwalkers*nwalkers ; ++i ) distsort[i] = i;
    std::sort( distsort.begin(), distsort.end(),
               [dist](int i, int j) {
                 if ( ( dist[i] == 0 ) && ( dist[j] > 0 ) ) return false;
                 if ( ( dist[i] > 0 ) && ( dist[j] == 0 ) ) return true;
                 return dist[i] < dist[j];
               } );
    // ****
    // logger->debug( "*** IN RECLUSTER, distances=" );
    // str.str("");
    // for ( int i = 0 ; i < nwalkers*nwalkers ; ++i ) {
    //   str << dist[distsort[i]] << " ";
    // }
    // logger->debug( str.str() );
    // ****
    // str.str("");
    // str << "Walker 5:";
    // for ( int i = 0 ; i < nparam ; ++i ) {
    //   str << " " << param[5][i];
    // }
    // logger->debug( str.str() );
    // str.str("");
    // str << "*** DISTANCES TO WALKER 5 ***\n";
    // for ( int i = 0 ; i < nwalkers ; ++i ) {
    //   str << "  " << std::setw(4) << i << " : " << dist[5*nwalkers+i] << std::endl;
    // }
    // logger->debug( str.str() );
    // ****

    // I don't really know the right way to do this.
    // Current thought:
    // We're going to consider  some factor * nparam as the cluster size
    // Rationale : if initsigma really is a good distance scale,
    // then it's like a chisquare
    dtype clusterradfac = 0.1;
    while ( dist[distsort[nwalkers]] > clusterradfac * nparam ) {
      str.str("");
      str << "dist[distsort[nwalkers]]=" << dist[distsort[nwalkers]]
          << " ; clusterradfac=" << clusterradfac
          << " ; clusterradfac*nparam=" << clusterradfac*nparam;
      logger->debug( str.str() );
      clusterradfac *= 1.2;
    }
    // Double the clusterrad we found
    clusterrad = clusterradfac * nparam * 2;
    str.str("");
    str << "clusterradfac=" << clusterradfac << " , clusterrad=" << clusterrad;
    logger->debug( str.str() );
    int nwithinclusterrad = 0;
    while ( ( nwithinclusterrad < distsort.size() ) && ( dist[distsort[nwithinclusterrad]] > 0 ) &&
            ( dist[distsort[nwithinclusterrad]] <= clusterrad ) )
      nwithinclusterrad++;
    str.str("");
    str << "There are " << nwithinclusterrad << " distances within " << clusterrad;
    logger->debug( str.str()) ;
    // if ( nwithinclusterrad < nwalkers ) {
    //   str.str("");
    //   clusterrad = dist[distsort[nwalkers-1]];
    //   str << "...expanding clusterrad to " << clusterrad;
    //   logger->debug( str.str() );
    // }

    int curcluster = -1;
    for ( int i = 0 ; i < nwalkers ; ++i ) {
      if ( label[i] != clusterundef ) continue;
      neighbors.clear();
      for ( int j = 0 ; j < nwalkers ; ++j ) if ( dist[i*nwalkers+j] <= clusterrad ) neighbors.push_back( j );
      if ( neighbors.size() < minclustersize ) {
        str.str("");
        str << "Walker " << i << ", only " << neighbors.size() << " neighbors which is <" << minclustersize;
        logger->debug( str.str() );
        label[i] = clusternoise;
        continue;
      }
      curcluster += 1;
      label[i] = curcluster;
      for ( int ndex = 0 ; ndex < neighbors.size() ; ++ndex ) {
        if ( neighbors[ndex] == i ) continue;
        int j = neighbors[ndex];
        if ( label[j] == clusternoise ) label[j] = curcluster;
        if ( label[j] != clusterundef ) continue;
        label[j] = curcluster;
        subneighbors.clear();
        for (int k = 0 ; k < nwalkers ; ++k ) if ( dist[j*nwalkers+k] <= clusterrad ) subneighbors.push_back( k );
        if ( subneighbors.size() >= minclustersize ) {
          for ( int k = 0 ; k < subneighbors.size() ; ++k ) {
            bool found = false;
            for ( int l = 0 ; l < neighbors.size() ; ++l ) {
              if ( neighbors[l] == subneighbors[k] ) {
                found = true;
                break;
              }
            }
            if ( !found ) neighbors.push_back( subneighbors[k] );
          }
        }
      }
    }
    int nclusters = curcluster+1;

    // Calculate size, mean lnL, stdev lnL for each cluster
    clustersize.resize( nclusters );
    clustermean.resize( nclusters );
    clustersdev.resize( nclusters );
    for ( int i = 0 ; i < nclusters ; ++i ) {
      clustersize[i] = 0;
      clustermean[i] = 0.;
      clustersdev[i] = 0.;
      for (int j = 0 ; j < nwalkers ; ++j ) {
        if ( label[j] == i ) {
          clustersize[i] += 1;
          clustermean[i] += lnL[j];
          clustersdev[i] += lnL[j] * lnL[j];
        }
      }
      clustermean[i] /= clustersize[i];
      clustersdev[i] = sqrt( clustersdev[i] / clustersize[i] - clustermean[i] * clustermean[i] );
    }

    // Sort clusters from high to low lnL
    clustersort.resize( nclusters );
    for ( int i = 0 ; i < nclusters ; ++i ) clustersort[i] = i;
    std::sort( clustersort.begin() , clustersort.end(),
               [clustermean](int i, int j) { return clustermean[i] > clustermean[j]; } );
    clusterorder.resize( nclusters );
    for ( int i = 0 ; i < nclusters ; ++i ) clusterorder[ clustersort[i] ] = i;

    // Debug output cluster info

    str.str( "" );
    str << "*** Reclustered ***" << std::endl;

    // Sort walkers by cluster from high to low lnL, with outliers at the end
    std::vector<int> walkerdex( nwalkers );
    for ( int i = 0 ; i < nwalkers ; ++i ) walkerdex[i] = i;
    std::sort( walkerdex.begin(), walkerdex.end(),
               [this,label,clusterorder](int i, int j) {
                 if ( ( label[i] < 0 ) && ( label[j] >= 0 ) ) return false;
                 if ( ( label[i] >= 0 ) && ( label[j] < 0 ) ) return true;
                 if ( label[i] == label[j] ) return this->lnL[i] > this->lnL[j];
                 return clusterorder[label[i]] < clusterorder[label[j]];
               } );
    curcluster = -999999;
    for ( int i = 0 ; i < nwalkers ; ++i ) {
      if ( label[walkerdex[i]] != curcluster ) {
        curcluster = label[walkerdex[i]];
        if ( curcluster >= 0 ) {
          str << "  ** Cluster " << curcluster << ": " << std::scientific << std::setprecision(4)
              << clustermean[curcluster] << " ± " << clustersdev[curcluster] << std::endl;
        } else {
          str << "  ** Outliers" << std::endl;
        }
      }
      str << "    Walker " << std::setw(4) << walkerdex[i]
          << " ; lnL = " << std::scientific << std::setprecision(4) << std::setw(11) << lnL[walkerdex[i]]
          << " ; cluster " << std::setw(3) << label[walkerdex[i]] ;
      str << " ; params";
      for ( int j = 0 ; j < nparam ; ++j )
        str << " " << std::setw(10) << param[walkerdex[i]][j];
      str << std::endl;
    }
    logger->info( str.str() );

    // Figure out the new paramter means and standard deviations from the
    // highest liklihood cluster.

    int ninfirstcluster = clustersize[ clustersort[0] ];
    for ( int j = 0 ; j < nparam ; ++j ) {
      if ( ! fixparam[j] ) {
        initparam[j] = 0.;
        initsigma[j] = 0.;
        for ( int i = 0 ; i < nwalkers ; ++i ) {
          if ( label[i] == clustersort[0] ) {
            initparam[j] += param[i][j];
            initsigma[j] += param[i][j] * param[i][j];
          }
        }
        initparam[j] /= ninfirstcluster;
        initsigma[j] = sqrt( ( initsigma[j] / ninfirstcluster - initparam[j] * initparam[j] ) / ninfirstcluster );
      }
    }

    str.str("");
    str << "Reclustered. " << nclusters << " clusters of walkers, "
        << ninfirstcluster << " in first with lnL=" << clustermean[clustersort[0]] << std::endl;
    logger->info( str.str() );

    str.str("");
    str << "New parameter centers: " << std::endl;
    for ( int j = 0 ; j < nparam ; ++j ) {
      str << "  param " << std::setw(4) << j << " : "
          << std::setw(11) << initparam[j] << " ± " << std::setw(11) << initsigma[j] << std::endl;
    }
    logger->info( str.str() );
  }
  MPI_Bcast( initparam.data(), nparam, mpitype, 0, comm );
  MPI_Bcast( initsigma.data(), nparam, mpitype, 0, comm );

  initialize_parameters();
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::step() {
  if ( comm == MPI_COMM_NULL ) return;

  std::stringstream str("");

  dtype r, z, x, acceptratio;
  int other, yes;

  // Do stretch move.
  //
  // See Goodman & Weare p. 69; the complementary set for walker number
  //   wlkr is the current state for walkers wlkr+1 through the end,
  //   but the updated state for walkers 0 through wlkr-1
  //
  // In order to parallelize this, follow Foreman-Mackey 2012.  Divide
  //   the walkers into two sets, S1 and S2, where S1 has the first half
  //   of walkers and S2 has the second half.  First do all of S1 in
  //   parallel, choosing only a walker from S2 as the source of its
  //   stretch move.  Then, do it the other way around.  This ensures
  //   that each walker chooses another walker to base its move on from
  //   its proper complementary set, though it does mean that each
  //   walker chooses its stretch base from only ~half of its full
  //   complementary set.

  int wlkr0 = nwalkers/2;
  int wlkr1 = nwalkers;
  int otherwlkr0 = 0;
  int otherwlkr1 = nwalkers/2;
  int mywlkr0;
  int mywlkr1;
  int accepted_this_step = 0;
  for ( int pass = 0 ; pass < 2 ; ++pass ) {
    int temp = wlkr0;
    wlkr0 = otherwlkr0;
    otherwlkr0 = temp;
    temp = wlkr1;
    wlkr1 = otherwlkr1;
    otherwlkr1 = temp;

    if ( submpirank == 0 ) {
      mywlkr0 = wlkr0 + mpirank * (nwalkers/2) / mpisize;
      mywlkr1 = ( mpirank == mpisize-1 ) ? wlkr1 : wlkr0 + (mpirank+1) * (nwalkers/2) / mpisize;
      if ( true ) {
        std::stringstream str("");
        str << "Rank " << mpirank << " (global " << globalmpirank << ") doing walkers "
            << "[" << mywlkr0 << ", " << mywlkr1 << ")";
        logger->debug( str.str() );
      }
    }
    MPI_Bcast( &mywlkr0, 1, MPI_INT, 0, subcomm );
    MPI_Bcast( &mywlkr1, 1, MPI_INT, 0, subcomm );

    for ( int wlkr = mywlkr0 ; wlkr < mywlkr1 ; ++wlkr ) {
      step_accepted[wlkr] = 0;
      if ( submpirank == 0 ) {
        other = otherwlkr0 + (int)( gsl_rng_uniform( rng ) * (otherwlkr1 - otherwlkr0) );
        others[wlkr] = other;

        // Generate the stretch slope z.  We want probability density of
        //   z to be ∝ 1/√z for z=[1/a, a] ( a = stretchparam )
        // This is a uniform x for x = 2√z
        x = gsl_rng_uniform( rng ) * stretchxr + stretchx0;
        z = x*x / 4.;
        zs[wlkr] = z;

        for ( int i = 0 ; i < nparam ; ++i ) {
          if ( fixparam[i] )
            newparam[wlkr][i] = param[wlkr][i];
          else
            newparam[wlkr][i] = param[other][i]  + z * ( param[wlkr][i] - param[other][i] );
        }
      }
      // Share the parameters between all of the subranks that go with this rank
      MPI_Bcast( newparam[wlkr].data(), nparam, mpitype, 0, subcomm );
      MPI_Bcast( others.data() + wlkr, 1, MPI_SHORT, 0, subcomm );
      MPI_Bcast( zs.data() + wlkr, 1, mpitype, 0, subcomm );

      newlnL[wlkr] = loglikelihood( newparam[wlkr], subcomm ) * beta;

      if ( submpirank == 0 ) {
        acceptratio = (nparam-1) * log(z) + newlnL[wlkr] - lnL[wlkr];
        if ( acceptratio >= 0 ) {
          acceptratio = 1.;
          yes = 1;
        }
        else {
          acceptratio = exp( acceptratio );
          r = gsl_rng_uniform( rng );
          yes = ( r < acceptratio ) ? 1 : 0;
        }
      }
      MPI_Bcast( &yes, 1, MPI_INT, 0, subcomm );
      if ( yes > 0 ) {
        step_accepted[wlkr] = 1;
        accepted_this_step += 1;
        // Update the walker to the new position.  This will make its new
        //   position part of the complementary set for higher-numbered
        //   walkers
        for ( int i = 0 ; i < nparam ; ++i ) param[wlkr][i] = newparam[wlkr][i];
        lnL[wlkr] = newlnL[wlkr];
      }

      if ( false ) {
        std::stringstream str("");
        str << "Walker " << wlkr << ( yes ? " step accepted " : " step rejected." ) << std::endl;
        str << "   other = " << other << " with x= " << x << " and z=" << z << std::endl;
        str << "   z = " << z << "  r = "  << r << "  acceptratio = " << acceptratio << std::endl;
        str << "   ...newlnL = " << newlnL[wlkr] << " , lnL = " << lnL[wlkr] << std::endl;
        str << "   ...param = ";
        for ( int i = 0 ; i < nparam ; ++i ) {
          str << param[wlkr][i] << "  ";
        }
        str << "\n   ...newparam = ";
        for ( int i = 0 ; i < nparam ; ++i ) {
          str << newparam[wlkr][i] << "  ";
        }
        logger->debug( str.str() );
      }

    }

    // Spread out the updated parameters
    for ( int rank = 0 ; rank < mpisize ; ++rank ) {
      int subwlkr0 = wlkr0 + rank * (nwalkers/2) / mpisize;
      int subwlkr1 = ( rank == mpisize-1 ) ? wlkr1 : wlkr0 + (rank+1) * (nwalkers/2) / mpisize;
      if ( false ) {
        if ( rank == mpirank ) {
          std::stringstream str("");
          str << "Rank " << rank << " (global " << globalmpirank << ") is root for walkers ["
              << subwlkr0 << "," << subwlkr1 << ")";
          logger->debug( str.str() );
        }
      }
      if ( stumblercomm != MPI_COMM_NULL ) {
        for ( int wlkr = subwlkr0 ; wlkr < subwlkr1 ; ++wlkr ) {
          MPI_Bcast( param[wlkr].data(), nparam, mpitype, rank, stumblercomm );
          MPI_Bcast( newparam[wlkr].data(), nparam, mpitype, rank, stumblercomm );
        }
        MPI_Bcast( lnL.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, rank, stumblercomm );
        MPI_Bcast( newlnL.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, rank, stumblercomm );
        MPI_Bcast( others.data() + subwlkr0, subwlkr1-subwlkr0, MPI_SHORT, rank, stumblercomm );
        MPI_Bcast( zs.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, rank, stumblercomm );
        MPI_Bcast( step_accepted.data() + subwlkr0, subwlkr1-subwlkr0, MPI_SHORT, rank, stumblercomm );
      }
      for ( int wlkr = subwlkr0 ; wlkr < subwlkr1 ; ++wlkr ) {
        MPI_Bcast( param[wlkr].data(), nparam, mpitype, 0, subcomm );
        MPI_Bcast( newparam[wlkr].data(), nparam, mpitype, 0, subcomm );
      }
      MPI_Bcast( lnL.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, 0, subcomm );
      MPI_Bcast( newlnL.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, 0, subcomm );
      MPI_Bcast( others.data() + subwlkr0, subwlkr1-subwlkr0, MPI_SHORT, 0, subcomm );
      MPI_Bcast( zs.data() + subwlkr0, subwlkr1-subwlkr0, mpitype, 0, subcomm );
      MPI_Bcast( step_accepted.data() + subwlkr0, subwlkr1-subwlkr0, MPI_SHORT, 0, subcomm );
    }

  }
  wlkrsteps += nwalkers;

  if ( stumblercomm != MPI_COMM_NULL )
    MPI_Allreduce( MPI_IN_PLACE, &accepted_this_step, 1, MPI_INT, MPI_SUM, stumblercomm );
  MPI_Bcast( &accepted_this_step, 1, MPI_INT, 0, subcomm );
  naccepted += accepted_this_step;
}

// **********************************************************************

template<typename dtype>
void Stumbler<dtype>::go( int printevery ) {
  if ( comm == MPI_COMM_NULL ) return;

  std::stringstream str("");

  reinitialize( rngseed );
  steptime = std::chrono::duration<double>::zero();
  dumptime = std::chrono::duration<double>::zero();
  covartime = std::chrono::duration<double>::zero();
  std::list<int> naccepted_history;

  auto t0 = std::chrono::high_resolution_clock::now();

  while ( curstep < burnin + nsteps ) {
    if ( curstep == burnin ) naccepted_at_burnin = naccepted;
    if ( ( globalmpirank == 0 ) && ( printevery !=0 ) && ( curstep % printevery == 0 ) ) {
      dtype postbiratio = 0.;
      if ( curstep > burnin ) postbiratio = ( (dtype)(naccepted - naccepted_at_burnin)
                                              / (dtype)(wlkrsteps - burnin*nwalkers) );
      str.str("");
      str << "Step " << curstep-burnin << "/" << nsteps
          << "; β=" << std::fixed << std::setprecision(3) << beta
          << "; acc " << naccepted << "/" << wlkrsteps
          << " wlkrsteps ( " << std::setprecision(3) << std::fixed
          << ( wlkrsteps == 0 ? 0 : (dtype)naccepted/(dtype)wlkrsteps ) << ")"
          << " (" << postbiratio << " post-burn)";
      logger->info( str.str() );
      if ( debugdump != nullptr ) debugdump();
    }

    t0 = std::chrono::high_resolution_clock::now();
    step();
    steptime += std::chrono::high_resolution_clock::now() - t0;

    if ( globalmpirank == 0 ) {
      dtypewidechains["chain"]->append( param );
      if ( keepdebuginfo ) {
        dtypewidechains["proposed"]->append( newparam );
        dtypechains["lnL"]->append( lnL );
        dtypechains["proposed_lnL"]->append( newlnL );
        dtypechains["z"]->append( zs );
        shortchains["walker"]->append( walkers );
        shortchains["other"]->append( others );
        shortchains["accepted"]->append( step_accepted );
      }
    }

    if ( ( curstep < burnin ) && rutrecluster ) {
      short int mustrecluster = 0;
      if ( globalmpirank == 0 ) {
        naccepted_history.push_back( naccepted );
        if ( naccepted_history.size() >= rutlength ) {
          bool rut = true;
          for ( auto ai=naccepted_history.begin() ; ai!=naccepted_history.end() ; ++ai ) {
            if ( (*ai) != naccepted_history.front() ) {
              rut = false;
              break;
            }
          }
          if ( rut ) mustrecluster = 1;
        }
      }
      MPI_Bcast( &mustrecluster, 1, MPI_SHORT, 0, comm );

      if ( mustrecluster ) {
        if ( globalmpirank == 0 ) logger->warn( "Stuck in a rut.  Reclustering." );
        recluster();
        if ( globalmpirank == 0 ) naccepted_history.clear();
      }

      if ( globalmpirank == 0 )
        while( naccepted_history.size() >= rutlength ) naccepted_history.pop_front();
    }

    if ( anneal && ( curstep < burnin/2 ) ) {
      logbeta += logbetastep;
      if ( logbeta >= 0. ) beta = 1.;
      else beta = exp( logbeta );
    }
    else {
      beta = 1.;
    }
    if ( cluster && ( burnin > 0 ) && ( !reclustered ) && ( curstep >= 3*burnin/4 ) ) {
      recluster();
      reclustered = true;
    }

    t0 = std::chrono::high_resolution_clock::now();
    if ( ( curstep >= burnin ) & ( globalmpirank == 0 ) && ( ! do_not_calculate_covar ) ) {
      for ( int nw = 0 ; nw < nwalkers ; ++nw ) {
        for ( int i = 0 ; i < nparam ; ++i ) {
          working_mean[i] += param[nw][i];
          for ( int j = 0 ; j < nparam ; ++j ) {
            working_covar[i][j] += param[nw][i] * param[nw][j];
          }
        }
      }
    }
    covartime += std::chrono::high_resolution_clock::now() - t0;

    if ( ( dtypewidechains["chain"]->cur >= dumpchainevery ) || ( curstep+1 == burnin ) ) {
      t0 = std::chrono::high_resolution_clock::now();
      dump_chains( curstep );
      dumptime += std::chrono::high_resolution_clock::now() - t0;
    }

    curstep += 1;
  }
  t0 = std::chrono::high_resolution_clock::now();
  dump_chains( curstep );
  dumptime += std::chrono::high_resolution_clock::now() - t0;

  MPI_Barrier( comm );
  if ( globalmpirank == 0 ) {
    str.str("");
    str << "Finished.  " << curstep << " steps, saved " << curstep-burnin;
    logger->info( str.str() );
    dtype postbiratio = ( (dtype)(naccepted - naccepted_at_burnin) / (dtype)(wlkrsteps - burnin*nwalkers) );
    str.str("");
    str << "Accepted " << naccepted << " of " << wlkrsteps
        << " walker steps ( " << std::setprecision(3) << std::fixed
        << (dtype)naccepted/(dtype)wlkrsteps << " ) (post-burn: " << postbiratio << ").";
    logger->info(str.str());
    str.str("");
    str << "Step time was " << steptime.count() << ", dump time was " << dumptime.count()
        << ", covartime was " << covartime.count();
    logger->info(str.str() );

    // Reduce mean and covar
    if ( ! do_not_calculate_covar ) {
      logger->info( "Reducing mean and covar..." );
      for ( int i = 0 ; i < nparam ; ++i ) working_mean[i] /= nsteps * nwalkers;
      for ( int i = 0 ; i < nparam ; ++i ) {
        for ( int j = i ; j < nparam ; ++j ) {
          working_covar[i][j] = working_covar[i][j] / ( nsteps * nwalkers ) - working_mean[i] * working_mean[j];
          working_covar[j][i] = working_covar[i][j];
        }
      }
      logger->info( "...done" );
    }
  }
}

// **********************************************************************
// Will only return the data in the first rank of comm

template<typename dtype>
void Stumbler<dtype>::get_mean_var( std::vector<std::vector<dtype>> &mean_var ) {
  if ( ( comm == MPI_COMM_NULL ) || ( globalmpirank != 0 ) )  return;

  if ( do_not_calculate_covar )
    throw std::runtime_error( "Can't call get_mean_var if you set do_not_calculate_covar" );

  mean_var.resize(2);
  mean_var[0].resize(nparam);
  mean_var[1].resize(nparam);

  for ( int i = 0 ; i < nparam ; ++i ) {
    mean_var[0][i] = working_mean[i];
    mean_var[1][i] = working_covar[i][i];
  }
}

template<typename dtype>
void Stumbler<dtype>::get_covar( std::vector<std::vector<dtype>> &covar ) {
  if ( ( comm == MPI_COMM_NULL ) || ( globalmpirank != 0 ) ) return;

  if ( do_not_calculate_covar )
    throw std::runtime_error( "Can't call get_covar if you set do_not_calculate_covar" );

  covar.resize( nparam );
  for ( int i = 0 ; i < nparam ; ++i ) {
    covar[i].resize( nparam );
    for ( int j = 0 ; j < nparam ; ++j ) covar[i][j] = working_covar[i][j];
  }
}

// **********************************************************************
// Force instantiations

template class Stumbler<float>;
template class Stumbler<double>;

